package com.toyshopstudio.madmin.business;

import com.toyshopstudio.madmin.infra.MadminFormatTool;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Unit test suite for ${@link Box}.
 * Created by Saulo Junior on 8/1/2016.
 */
public class BoxTest {

    private final Box box = new Box();
    private final String description = "Food";

    @Test
    public void getFormattedOutgoSum() {
        box.setOutgoSum(BigDecimal.valueOf(15850.33));
        Assert.assertTrue(box.getFormattedOutgoSum().equals(
                MadminFormatTool.BalanceSymbol.OUTGO.getSymbol().concat("15,850.33"))
        );
    }

    @Test
    public void getFormattedOutgoSum_withNullValue() {
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedOutgoSum().equals(""));
    }

    @Test
    public void getFormattedOutgoSum_withZeroValue() {
        box.setOutgoSum(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedOutgoSum().equals(""));
    }

    @Test
    public void getFormattedIncomeSum() {
        box.setIncomeSum(BigDecimal.valueOf(1000));
        Assert.assertTrue(box.getFormattedIncomeSum().equals(
                MadminFormatTool.BalanceSymbol.INCOME.getSymbol().concat("1,000.00"))
        );
    }

    @Test
    public void getFormattedIncomeSum_withNullValue() {
        box.setIncomeSum(null);
        Assert.assertTrue(box.getFormattedIncomeSum().equals("")
        );
    }

    @Test
    public void getFormattedIncomeSum_withZeroValue() {
        box.setIncomeSum(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedIncomeSum().equals("")
        );
    }

    @Test
    public void getFormattedBalance() {
        box.setIncomeSum(BigDecimal.valueOf(1000));
        box.setOutgoSum(BigDecimal.valueOf(1584));
        Assert.assertTrue(box.getFormattedBalance().equals("-584.00"));
        box.setIncomeSum(BigDecimal.valueOf(1000));
        box.setOutgoSum(BigDecimal.valueOf(1000));
        Assert.assertTrue(box.getFormattedBalance().equals("0.00"));
        box.setIncomeSum(BigDecimal.valueOf(1000));
        box.setOutgoSum(BigDecimal.valueOf(1));
        Assert.assertTrue(box.getFormattedBalance().equals(
                MadminFormatTool.BalanceSymbol.POSITIVE.getSymbol().concat("999.00"))
        );
        box.setIncomeSum(null);
        box.setOutgoSum(BigDecimal.valueOf(1000));
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(BigDecimal.valueOf(1000));
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(null);
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(BigDecimal.ZERO);
        box.setOutgoSum(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
    }

    @Test
    public void getFormattedBalance_withNullValue() {
        box.setIncomeSum(null);
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(BigDecimal.valueOf(34));
        box.setBarSize(4);
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(null);
        box.setOutgoSum(BigDecimal.valueOf(324));
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(BigDecimal.ZERO);
        box.setOutgoSum(null);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
        box.setIncomeSum(null);
        box.setOutgoSum(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedBalance().equals(""));

    }

    @Test
    public void getFormattedBalance_withZeroValue() {
        box.setIncomeSum(BigDecimal.ZERO);
        box.setOutgoSum(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedBalance().equals(""));
    }

    @Test
    public void getFormattedMonthlyAverage() {
        final Box box = new Box();
        box.setOutgoMonthlyAverage(BigDecimal.valueOf(780));
        Assert.assertTrue(box.getFormattedMonthlyAverage().equals("780.00"));
    }

    @Test
    public void getFormattedMonthlyAverage_withNullValue() {
        box.setOutgoMonthlyAverage(null);
        Assert.assertTrue(box.getFormattedMonthlyAverage().equals(""));
    }

    @Test
    public void getFormattedMonthlyAverage_withZeroValue() {
        final Box box = new Box();
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        Assert.assertTrue(box.getFormattedMonthlyAverage().equals(""));
    }

    @Test
    public void getDescription_shorterThanMaxCharactersLimit() {
        box.setDescription(description);
        Assert.assertTrue(box.getDescription(description.length()+1).equals(description));
    }

    @Test
    public void getDescription_greaterThanMaxCharactersLimit() {
        box.setDescription(description);
        Assert.assertTrue(box.getDescription(description.length()-1)
                .equals(description.substring(0,description.length()-1).concat("...")));
    }

    @Test
    public void getDescription_equalsMaxCharactersLimit() {
        final Box box = new Box();
        box.setDescription(description);
        Assert.assertTrue(box.getDescription(description.length())
                .equals(description.substring(0,description.length())));
    }

    @Test
    public void getDescription_whenNull() {
        box.setDescription(null);
        Assert.assertTrue(box.getDescription(3).equals(""));
    }

    @Test
    public void getTableName() {
        Assert.assertTrue(Box.getTableName().equals("label"));
    }

    @Test
    public void equals(){
        Assert.assertTrue(new Outgo().equals(new Outgo()));
        box.setDescription("Health");
        Assert.assertFalse(new Box().equals(box));
        box.setDescription(null);
        Assert.assertTrue(box.equals(new Box()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void _hashCode(){
        Set incomes = new HashSet<Box>(0);
        incomes.add(box);
        Assert.assertTrue(incomes.contains(box));
    }
}
