package com.toyshopstudio.madmin.business;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Unit tests for any class inherited from Entry.
 * Created by Saulo Junior on 7/29/2016.
 */
public class EntryTest {

    private final String description = "Pan Kanapka";
    private final Entry income = new Income();
    private final Entry outgo = new Outgo();


    @Test
    public void getFormattedValue_forIncome() {
        income.setValue(BigDecimal.valueOf(1000));
        Assert.assertTrue(income.getFormattedValue().equals("1,000.00"));
    }

    @Test
    public void getFormattedValue_forOutgo() {
        outgo.setValue(BigDecimal.valueOf(1553.87));
        Assert.assertTrue(outgo.getFormattedValue().equals("1,553.87"));
    }

    @Test
    public void getFormattedValue_forNullValue() {
        Assert.assertTrue(outgo.getFormattedValue().equals("0.00"));
    }

    @Test
    public void getFormattedValue_forZeroValue() {
        outgo.setValue(BigDecimal.ZERO);
        Assert.assertTrue(outgo.getFormattedValue().equals("0.00"));
    }

    @Test
    public void getFormattedValue_forNegativeValue() {
        income.setValue(BigDecimal.valueOf(-10000.44));
        Assert.assertTrue(income.getFormattedValue().equals("-10,000.44"));
    }

    @Test
    public void getDescription_shorterThanMaxCharactersLimit() {
        income.setDescription(description);
        Assert.assertTrue(income.getDescription(description.length()+1).equals(description));
    }

    @Test
    public void getDescription_greaterThanMaxCharactersLimit() {
        income.setDescription(description);
        Assert.assertTrue(income.getDescription(description.length()-1)
                .equals(description.substring(0,description.length()-1).concat("...")));
    }

    @Test
    public void getDescription_equalsMaxCharactersLimit() {
        income.setDescription(description);
        Assert.assertTrue(income.getDescription(description.length())
                .equals(description.substring(0,description.length())));
    }

    @Test
    public void getDescription_whenNull() {
        income.setDescription(null);
        Assert.assertTrue(income.getDescription(5).equals(""));
    }

    @Test
    public void getTableName() {
        Assert.assertTrue(Outgo.getTableName().equals("entry"));
        Assert.assertTrue(Income.getTableName().equals("entry"));
    }

    @Test
    public void equals(){
        Assert.assertTrue(new Outgo().equals(new Outgo()));
        Assert.assertFalse(income.equals(new Outgo()));
        Assert.assertFalse(income.equals(outgo));
        income.setValue(BigDecimal.TEN);
        Assert.assertFalse(new Outgo().equals(income));
        income.setDescription(null);
        income.setId(1L);
        income.setBox(new Box());
        income.setDateCreated(new Date());
        income.setValue(BigDecimal.TEN);
        Assert.assertFalse(income.equals(new Income()));
        Entry inc = new Income();
        inc.setValue(income.getValue());
        inc.setDescription(income.getDescription());
        inc.setId(income.getId());
        inc.setDateCreated(income.getDateCreated());
        inc.setBox(income.getBox());
        Assert.assertTrue(income.equals(inc));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void _hashCode(){
        Set incomes = new HashSet<Entry>(0);
        incomes.add(income);
        Assert.assertTrue(incomes.contains(income));
    }

}
