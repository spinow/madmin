package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.model.AppConfigRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppConfigServiceTest {

    @Mock
    private AppConfigRepository appConfigRepository = Mockito.mock(AppConfigRepository.class);

    @Test(expected=RuntimeException.class)
    public void defaultConstructor(){
        new AppConfigService();
    }

    @Test
    public void increaseAccessCount(){
        AppConfig appConfig = new AppConfig();
        appConfig.setAccessCounter(0L);
        when(appConfigRepository.find()).thenReturn(appConfig);
        when(appConfigRepository.save(appConfig)).thenReturn(new AppConfig());
        AppConfigService appConfigService = AppConfigService.newInstance(appConfigRepository);
        appConfigService.increaseAccessCount();
        Assert.assertTrue(appConfig.getAccessCounter().equals(1L));
        appConfigService.increaseAccessCount();
        Assert.assertTrue(appConfig.getAccessCounter().equals(2L));
        appConfigService.increaseAccessCount();
        Assert.assertTrue(appConfig.getAccessCounter().equals(3L));
    }
}
