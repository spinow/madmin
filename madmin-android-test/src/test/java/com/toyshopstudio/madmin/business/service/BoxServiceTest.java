package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.MadminFormatTool;
import com.toyshopstudio.madmin.model.BoxRepository;
import static org.mockito.Mockito.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * Created by Saulo Junior on 8/4/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class BoxServiceTest {

    @Mock
    private BoxRepository boxRepository = Mockito.mock(BoxRepository.class);
    private BoxService boxService;

    @Test
    public void getOutgoSum(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(120));
        Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(100));
        Collection<Box> boxes = new ArrayList<Box>(0);
        boxes.add(healthBox);
        boxes.add(educationBox);
        Assert.assertTrue(boxService.getOutgoSum(boxes).equals(BigDecimal.valueOf(220)));
    }

    @Test
    public void getBoxOutgoPercentageInPeriod(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(120));
        Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getBoxOutgoPercentageInPeriod(
                        healthBox.getOutgoSum().add(educationBox.getOutgoSum()), healthBox
                ).equals(12F)
        );
    }

    @Test
    public void getBoxOutgoPercentageInPeriod_withBoxNullValue(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(null);
        Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getBoxOutgoPercentageInPeriod(
                        educationBox.getOutgoSum(), healthBox
                ).equals(0F)
        );
    }

    @Test
    public void getBoxOutgoPercentageInPeriod_withBoxZeroValue(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box(1L);
        healthBox.setOutgoSum(BigDecimal.ZERO);
        Box educationBox = new Box(2L);
        educationBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getBoxOutgoPercentageInPeriod(
                        healthBox.getOutgoSum().add(educationBox.getOutgoSum()), healthBox
                ).equals(0F)
        );
    }

    @Test
    public void getBoxOutgoPercentageInPeriod_withPeriodNullValue(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(880));
        Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getBoxOutgoPercentageInPeriod(
                        null, healthBox
                ).equals(0F)
        );
    }

    @Test
    public void getBoxOutgoPercentageInPeriod_withPeriodZeroValue(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(880));
        Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getBoxOutgoPercentageInPeriod(
                        BigDecimal.ZERO, healthBox
                ).equals(0F)
        );
    }

    @Test
    public void getFormattedOutgoSum_withOutgoSum(){
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(880));
        Assert.assertTrue(
                boxService.getFormattedOutgoSum(healthBox.getOutgoSum(), null).equals(MadminFormatTool.BalanceSymbol.OUTGO.getSymbol().concat("880.00"))
        );
    }

    @Test
    public void getFormattedOutgoSum_withoutOutgoSum() throws ParseException {
        DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        when(boxRepository.sumOutgo(any(DatePeriod.class))).thenReturn(BigDecimal.valueOf(500));
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        Assert.assertTrue(
                boxService.getFormattedOutgoSum(healthBox.getOutgoSum(), datePeriod).equals(MadminFormatTool.BalanceSymbol.OUTGO.getSymbol().concat("500.00"))
        );
    }

    @Test
    public void getFormattedOutgoSum_withZeroOutgoSum() throws ParseException {
        DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        when(boxRepository.sumOutgo(any(DatePeriod.class))).thenReturn(BigDecimal.valueOf(0));
        boxService = BoxService.newInstance(boxRepository);
        Box healthBox = new Box();
        Assert.assertTrue(
                boxService.getFormattedOutgoSum(healthBox.getOutgoSum(), datePeriod).equals("")
        );
    }

    @Test
    public void getAll() throws ParseException {
        final Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(500));
        healthBox.setOutgoMonthlyAverage(BigDecimal.valueOf(500));
        final Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(500));
        educationBox.setOutgoMonthlyAverage(BigDecimal.valueOf(500));
        final Collection<Box> boxes = new ArrayList<Box>(0);
        boxes.add(healthBox);
        boxes.add(educationBox);
        when(boxRepository.sumOutgo(any(DatePeriod.class))).thenReturn(healthBox.getOutgoSum().add(educationBox.getOutgoSum()));
        when(boxRepository.findAll(any(DatePeriod.class))).thenReturn(boxes);
        boxService = BoxService.newInstance(boxRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Collection<Box> outcomeList = boxService.getAll(datePeriod);
        Assert.assertTrue(
                outcomeList.iterator().next().getOutgoPercentageInPeriod().equals(50F)
        );
    }

    @Test
    public void getById(){
        final Box healthBox = new Box();
        healthBox.setId(1L);
        healthBox.setBarSize(100);
        when(boxRepository.findById(1L,null)).thenReturn(healthBox);
        boxService = BoxService.newInstance(boxRepository);
        final Box box = boxService.getById(healthBox.getId(),null);
        Assert.assertTrue(
                box.getId().equals(healthBox.getId()) && box.getBarSize().equals(healthBox.getBarSize())
        );
    }

    @Test
    public void delete(){
        final Box healthBox = new Box();
        healthBox.setId(1L);
        when(boxRepository.delete(healthBox)).thenReturn(true);
        boxService = BoxService.newInstance(boxRepository);
        final boolean deleted = boxService.delete(healthBox);
        Assert.assertTrue(
                deleted
        );
    }

    @Test
    public void save(){
        final Box healthBox = new Box();
        healthBox.setId(1L);
        when(boxRepository.save(any(Box.class))).thenReturn(healthBox);
        boxService = BoxService.newInstance(boxRepository);
        final Box saved = boxService.save(new Box());
        Assert.assertTrue(
                saved.getId().equals(1L)
        );
    }

    @Test
    public void getByDescription(){
        final Box healthBox = new Box();
        healthBox.setId(1L);
        healthBox.setDescription("Health");
        when(boxRepository.findByDescription(healthBox.getDescription())).thenReturn(healthBox);
        boxService = BoxService.newInstance(boxRepository);
        Box box = boxService.getByDescription(healthBox.getDescription());
        Assert.assertTrue(
                box.getId().equals(healthBox.getId())
        );
        box = boxService.getByDescription("Education");
        Assert.assertNull(box);
    }

    @Test(expected=RuntimeException.class)
    public void defaultConstructor(){
        new BoxService();
    }
}
