package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.Income;
import com.toyshopstudio.madmin.business.Outgo;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.MadminFormatTool;
import com.toyshopstudio.madmin.model.EntryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/4/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EntryServiceTest {

    @Mock
    private EntryRepository entryRepository = Mockito.mock(EntryRepository.class);

    @Test
    public void sumIncomes(){
        Collection<Entry> incomes = new ArrayList<Entry>(0);
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        Entry extraSalary = new Income();
        extraSalary.setValue(BigDecimal.valueOf(2000));
        incomes.add(salary);
        incomes.add(extraSalary);
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.sumIncomes(incomes).equals(BigDecimal.valueOf(5000)));
    }

    @Test
    public void sumIncomes_withOutgoTypes(){
        Collection<Entry> incomes = new ArrayList<Entry>(0);
        Entry expenses = new Outgo();
        expenses.setValue(BigDecimal.valueOf(3000));
        Entry extraExpenses = new Outgo();
        extraExpenses.setValue(BigDecimal.valueOf(2000));
        incomes.add(expenses);
        incomes.add(extraExpenses);
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.sumIncomes(incomes).equals(BigDecimal.valueOf(0)));
    }

    @Test
    public void getFormattedSelectedPeriodBalance(){
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        Entry extraSalary = new Income();
        extraSalary.setValue(BigDecimal.valueOf(2000));
        Entry expenses = new Outgo();
        expenses.setValue(BigDecimal.valueOf(2000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                salary.getValue().add(extraSalary.getValue()),
                expenses.getValue()).equals(MadminFormatTool.BalanceSymbol.POSITIVE.getSymbol().concat("3,000.00")
                )
        );
    }

    @Test
    public void getFormattedSelectedPeriodBalance_withNullIncomeSum(){
        Entry expenses = new Outgo();
        expenses.setValue(BigDecimal.valueOf(2000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                null,
                expenses.getValue()).equals(""
                )
        );
    }

    @Test
    public void getFormattedSelectedPeriodBalance_withZeroIncomeSum(){
        Entry expenses = new Outgo();
        expenses.setValue(BigDecimal.valueOf(2000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                BigDecimal.ZERO,
                expenses.getValue()).equals(""
                )
        );
    }

    @Test
    public void getFormattedSelectedPeriodBalance_withNullOutgoSum(){
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                salary.getValue(),
                null).equals("")
        );
    }

    @Test
    public void getFormattedSelectedPeriodBalance_withZeroOutgoSum(){
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                salary.getValue(),
                BigDecimal.ZERO).equals("")
        );
    }

    @Test
    public void getFormattedSelectedPeriodBalance_withNegativeBalance(){
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        Entry extraSalary = new Income();
        extraSalary.setValue(BigDecimal.valueOf(2000));
        Entry expenses = new Outgo();
        expenses.setValue(BigDecimal.valueOf(6000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        Assert.assertTrue(entryService.getFormattedSelectedPeriodBalance(
                salary.getValue().add(extraSalary.getValue()),
                expenses.getValue()).equals("-1,000.00")
        );
    }

    @Test
    public void getFormattedSelectedPeriodIncomeSum() throws ParseException {
        Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        Entry extraSalary = new Income();
        extraSalary.setValue(BigDecimal.valueOf(2000));
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        Assert.assertTrue(entryService.getFormattedSelectedPeriodIncomeSum(
                    salary.getValue().add(extraSalary.getValue()), datePeriod
                ).equals(MadminFormatTool.BalanceSymbol.INCOME.getSymbol().concat("5,000.00")
                )
        );
    }

    @Test
    public void getFormattedSelectedPeriodIncomeSum_withNullValue() throws ParseException {
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        Assert.assertTrue(entryService.getFormattedSelectedPeriodIncomeSum(
                null, datePeriod
                ).equals("")
        );
    }

    @Test
    public void getFormattedSelectedPeriodIncomeSum_withZeroValue() throws ParseException {
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        Assert.assertTrue(entryService.getFormattedSelectedPeriodIncomeSum(
                BigDecimal.ZERO, datePeriod
                ).equals("")
        );
    }

    @Test
    public void getFormattedSelectedPeriodIncomeSum_withDatePeriodAsParameterSignature() throws ParseException {
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        Assert.assertTrue(entryService.getFormattedSelectedPeriodIncomeSum(
                datePeriod).equals("")
        );
    }

    @Test
    public void delete(){
        final Entry healthBox = new Outgo();
        healthBox.setId(1L);
        when(entryRepository.delete(healthBox)).thenReturn(true);
        EntryService entryService = EntryService.newInstance(entryRepository);
        final boolean deleted = entryService.delete(healthBox);
        Assert.assertTrue(
                deleted
        );
    }

    @Test
    public void save(){
        final Entry healthBox = new Outgo();
        healthBox.setId(1L);
        when(entryRepository.save(any(Entry.class))).thenReturn(healthBox);
        EntryService entryService = EntryService.newInstance(entryRepository);
        final Entry saved = entryService.save(new Outgo());
        Assert.assertTrue(
                saved.getId().equals(1L)
        );
    }

    @Test
    public void getAll() throws ParseException {
        final Entry medicine = new Outgo();
        medicine.setValue(BigDecimal.valueOf(50));
        final Entry book = new Outgo();
        book.setValue(BigDecimal.valueOf(19));
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(medicine);
        expenses.add(book);
        when(entryRepository.findAllIncomes(any(DatePeriod.class))).thenReturn(expenses);
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Collection<Entry> outcomeList = entryService.getAllIncomes(datePeriod);
        Assert.assertTrue(
                outcomeList.containsAll(expenses)
        );
    }

    @Test
    public void getAllByBox() throws ParseException {
        Box educationBox  = new Box();
        final Entry museumTicket = new Outgo();
        museumTicket.setBox(educationBox);
        museumTicket.setValue(BigDecimal.valueOf(5));
        final Entry lunch = new Outgo();
        lunch.setBox(new Box());
        lunch.setValue(BigDecimal.valueOf(50));
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(museumTicket);
        when(entryRepository.findAllByBox(Matchers.refEq(educationBox), any(DatePeriod.class))).thenReturn(expenses);
        EntryService entryService = EntryService.newInstance(entryRepository);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Collection<Entry> outcomeList = entryService.getAllByBox(educationBox, datePeriod);
        Assert.assertTrue(
                outcomeList.contains(museumTicket) && !outcomeList.contains(lunch)
        );
    }

    @Test(expected=RuntimeException.class)
    public void defaultConstructor(){
        new EntryService();
    }
}
