package com.toyshopstudio.madmin.infra;

import android.accounts.Account;
import android.accounts.AccountManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/24/2016.
 */
//@RunWith(MockitoJUnitRunner.class)
public class AccountsToolTest {

    private static class MockAccount extends Account {
        public static final Creator<Account> CREATOR = null;

        MockAccount(String name, String type) {
            super(name, type);
        }

    }

    @Mock
    private AccountManager accountManager = Mockito.mock(AccountManager.class);

    /*@Test
    public void getAccountEmail () {
        final String email = "saulojoonior@gmail.com";
        Account account = new MockAccount(email, AccountManager.KEY_ACCOUNT_TYPE);
        Account[] accounts = new Account[]{
            account
        };
        when(accountManager.getAccounts()).thenReturn(accounts);
        final String outcome = AccountsTool.getAccountEmail(accountManager);
        Assert.assertTrue(outcome.equals(email));
    }*/
}
