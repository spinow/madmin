package com.toyshopstudio.madmin.infra;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

/**
 *
 * Created by Saulo Junior on 8/23/2016.
 */
public class DatePeriodTest {

    @Test(expected = ParseException.class)
    public void constructor_withInvalidDateFormat () throws ParseException {
        new DatePeriod("2016/1/1","2016/");
    }

    @Test
    public void isSelectedPeriodCurrent(){
        DatePeriod datePeriod = DatePeriod.getSelectedPeriod(null);
        Assert.assertTrue(
                DatePeriod.isSelectedPeriodCurrent(datePeriod)
        );
        Assert.assertFalse(
                DatePeriod.isSelectedPeriodCurrent(new DatePeriod(new Date(), new Date()))
        );
        Assert.assertTrue(
                DatePeriod.isSelectedPeriodCurrent(null)
        );
    }
}
