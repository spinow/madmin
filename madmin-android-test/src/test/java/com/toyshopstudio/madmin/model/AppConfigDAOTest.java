package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.infra.data.ContentValuesHolder;
import com.toyshopstudio.madmin.infra.data.DataBase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/16/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppConfigDAOTest {

    @Mock
    private final DataBase db = Mockito.mock(DataBase.class);
    @Mock
    private final Cursor cursor = Mockito.mock(Cursor.class);
    private AppConfigDAO appConfigDAO;
    private static long counter;

    private void setup() {
        counter += 1;
        final AppConfig config = new AppConfig();
        config.setId(1L);
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        when(db.update(any(String.class),any(ContentValuesHolder.class))).thenReturn(true);
        when(db.save(any(String.class),any(ContentValuesHolder.class))).thenReturn(true);
        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getLong(0)).thenReturn(1L);
        when(cursor.getLong(1)).thenReturn(counter);
        appConfigDAO = AppConfigDAO.newInstance(db);
    }

    @Test
    public void find(){
        setup();
        AppConfig appConfig = appConfigDAO.find();
        Assert.assertTrue(
                appConfig.getId() != null && appConfig.getAccessCounter().equals(counter)
        );
    }

    @Test
    public void save(){
        setup();
        AppConfig appConfig = appConfigDAO.save(new AppConfig());
        Assert.assertTrue(
                appConfig.getId() != null && appConfig.getAccessCounter().equals(counter)
        );
    }

    @Test
    public void save_anExistingAppConfig(){
        setup();
        AppConfig appConfig = new AppConfig();
        appConfig.setId(1L);
        when(db.save(any(String.class),any(ContentValuesHolder.class))).thenReturn(true);
        AppConfig outcome = appConfigDAO.save(appConfig);
        Assert.assertTrue(
                outcome.getId().equals(appConfig.getId()) && outcome.getAccessCounter().equals(counter)
        );
    }

}
