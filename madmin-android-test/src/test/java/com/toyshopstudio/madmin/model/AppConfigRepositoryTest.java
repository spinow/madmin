package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.business.Entry;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/16/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppConfigRepositoryTest {

    @Mock
    private final AppConfigDAO appConfigDAO = Mockito.mock(AppConfigDAO.class);

    @Test
    public void find(){
        final AppConfig config = new AppConfig();
        config.setId(1L);
        when(appConfigDAO.find()).thenReturn(config);
        AppConfig outcome = AppConfigRepository.newInstance(appConfigDAO).find();
        Assert.assertTrue(
                outcome.getId().equals(config.getId())
        );
    }

    @Test
    public void save(){
        final AppConfig config = new AppConfig();
        config.setId(1L);
        when(appConfigDAO.save(argThat(new ArgumentMatcher<AppConfig>() {
            public boolean matches(Object o) {
                return (null == ((AppConfig)o).getId()) || ((AppConfig)o).getId().equals(0L);
            }
        }))).thenReturn(config);
        AppConfig outcome = AppConfigRepository.newInstance(appConfigDAO).save(new AppConfig());
        Assert.assertTrue(
                outcome.getId().equals(config.getId())
        );
    }

    @Test
    public void save_anExistingAppConfig(){
        final AppConfig config = new AppConfig();
        config.setId(2L);
        when(appConfigDAO.save(argThat(new ArgumentMatcher<AppConfig>() {
            public boolean matches(Object o) {
                return (null != ((AppConfig)o).getId()) && !(((AppConfig)o).getId().equals(0L));
            }
        }))).thenReturn(config);
        AppConfig outcome = AppConfigRepository.newInstance(appConfigDAO).save(config);
        Assert.assertTrue(
                outcome.getId().equals(config.getId())
        );
    }
}
