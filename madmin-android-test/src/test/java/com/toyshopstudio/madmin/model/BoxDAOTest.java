package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.business.Outgo;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.DataBase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/17/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class BoxDAOTest {

    @Mock
    private final DataBase db = Mockito.mock(DataBase.class);
    @Mock
    private final Cursor cursor = Mockito.mock(Cursor.class);
    private BoxDAO boxDAO;
    @Mock
    private EntryDAO entryDAO = Mockito.mock(EntryDAO.class);

    @Test
    public void save(){
        final Box box = new Box();
        box.setId(null);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getLong(0)).thenReturn(1L);
        when(cursor.getString(1)).thenReturn(box.getDescription());
        when(cursor.getDouble(2)).thenReturn(box.getOutgoSum().doubleValue());
        when(cursor.getDouble(3)).thenReturn(box.getOutgoMonthlyAverage().doubleValue());
        when(cursor.getDouble(4)).thenReturn(box.getIncomeSum().doubleValue());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final Box outcome = boxDAO.save(box);

        Assert.assertTrue(
                outcome.getId().equals(1L) && outcome.getDescription().equals(box.getDescription())
        );
    }

    @Test
    public void save_anExistingBox(){
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getLong(0)).thenReturn(box.getId());
        when(cursor.getString(1)).thenReturn(box.getDescription());
        when(cursor.getDouble(2)).thenReturn(box.getOutgoSum().doubleValue());
        when(cursor.getDouble(3)).thenReturn(box.getOutgoMonthlyAverage().doubleValue());
        when(cursor.getDouble(4)).thenReturn(box.getIncomeSum().doubleValue());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final Box outcome = boxDAO.save(box);

        Assert.assertTrue(
                outcome.getId().equals(box.getId()) && outcome.getDescription().equals(box.getDescription())
        );
    }

    @Test
    public void findAll(){
        final Box healthBox = new Box();
        healthBox.setId(3L);
        healthBox.setDescription("Health");
        healthBox.setOutgoSum(BigDecimal.ZERO);
        healthBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        healthBox.setIncomeSum(BigDecimal.ZERO);

        final Box educationBox = new Box();
        educationBox.setId(4L);
        educationBox.setDescription("Education");
        educationBox.setOutgoSum(BigDecimal.TEN);
        educationBox.setOutgoMonthlyAverage(BigDecimal.TEN);
        educationBox.setIncomeSum(BigDecimal.TEN);

        final Collection<Box> boxes = new ArrayList<Box>(0);
        boxes.add(healthBox);
        boxes.add(educationBox);

        when(cursor.moveToNext()).thenReturn(true, true, false);
        when(cursor.getLong(0)).thenReturn(healthBox.getId(),educationBox.getId());
        when(cursor.getString(1)).thenReturn(healthBox.getDescription(),educationBox.getDescription());
        when(cursor.getDouble(2)).thenReturn(healthBox.getOutgoSum().doubleValue(),educationBox.getOutgoSum().doubleValue());
        when(cursor.getDouble(3)).thenReturn(healthBox.getOutgoMonthlyAverage().doubleValue(),educationBox.getOutgoMonthlyAverage().doubleValue());
        when(cursor.getDouble(4)).thenReturn(healthBox.getIncomeSum().doubleValue(),educationBox.getIncomeSum().doubleValue());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final Collection<Box> outcome = boxDAO.findAll();

        Assert.assertTrue(
                boxes.toArray()[0].equals(outcome.toArray()[0]) && boxes.toArray()[1].equals(outcome.toArray()[1])
        );
    }

    @Test
    public void sum() throws ParseException {
        final Box healthBox = new Box();
        healthBox.setId(3L);
        healthBox.setDescription("Health");
        healthBox.setOutgoSum(BigDecimal.ONE);

        final Box educationBox = new Box();
        educationBox.setId(4L);
        educationBox.setDescription("Education");
        educationBox.setOutgoSum(BigDecimal.TEN);

        final Collection<Box> boxes = new ArrayList<Box>(0);
        boxes.add(healthBox);
        boxes.add(educationBox);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getDouble(0)).thenReturn(
                healthBox.getOutgoSum().add(educationBox.getOutgoSum()).doubleValue()
        );
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final BigDecimal outcome = boxDAO.sum(EntryType.OUTGO,new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                outcome.compareTo(((Box)boxes.toArray()[0]).getOutgoSum().add(((Box)boxes.toArray()[1]).getOutgoSum())) == 0
        );
    }

    @Test
    public void findById() throws ParseException {
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getLong(0)).thenReturn(box.getId());
        when(cursor.getString(1)).thenReturn(box.getDescription());
        when(cursor.getDouble(2)).thenReturn(box.getOutgoSum().doubleValue());
        when(cursor.getDouble(3)).thenReturn(box.getOutgoMonthlyAverage().doubleValue());
        when(cursor.getDouble(4)).thenReturn(box.getIncomeSum().doubleValue());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final Box outcome = boxDAO.findById(box.getId(), new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                outcome.equals(box)
        );
    }

    @Test
    public void delete(){
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setBox(box);
        final Entry vitamins = new Outgo();
        medicines.setBox(box);
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(medicines);
        expenses.add(vitamins);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getInt(0)).thenReturn(EntryType.OUTGO.getNumber());
        when(db.delete(Box.getTableName(),box.getId())).thenReturn(true);
        when(db.delete(Box.getTableName(),null)).thenReturn(false);
        when(cursor.moveToNext()).thenReturn(true, false);
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        when(entryDAO.findAllByBox(any(Box.class))).thenReturn(expenses);
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        final boolean firstOutcome = boxDAO.delete(box);
        final boolean secondOutcome = boxDAO.delete(new Box());

        Assert.assertTrue(
                firstOutcome && (!secondOutcome)
        );
    }

    @Test(expected=NotImplementedException.class)
    public void findAllIncome() throws ParseException {
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        boxDAO.findAllIncome(new DatePeriod("1/1/2016","31/12/2016"));
    }

    @Test(expected=NotImplementedException.class)
    public void findAllBy() throws ParseException {
        boxDAO = BoxDAO.newInstance(db, entryDAO);
        boxDAO.findAllBy(new Box(),new DatePeriod("1/1/2016","31/12/2016"));
    }
}
