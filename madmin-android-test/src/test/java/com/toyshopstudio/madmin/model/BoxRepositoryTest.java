package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/11/2016.
 */
public class BoxRepositoryTest {

    @Mock
    private final BoxDAO boxDAO = Mockito.mock(BoxDAO.class);

    @Test(expected=RuntimeException.class)
    public void defaultConstructor(){
        new BoxRepository();
    }

    @Test
    public void save(){
        Box health = new Box();
        health.setId(2L);
        Box education = new Box();
        education.setId(1L);

        when(boxDAO.save(argThat(new ArgumentMatcher<Box>() {
            public boolean matches(Object o) {
                return (null == ((Box)o).getId()) || ((Box)o).getId().equals(0L);
            }
        }))).thenReturn(education);

        final Repository<Box> boxRepository = BoxRepository.newInstance(boxDAO);
        Assert.assertTrue(boxRepository.save(new Box()).getId().equals(1L));
    }

    @Test
    public void findById() throws ParseException {
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Map<Long, Box> allBoxes = new HashMap<Long, Box>(0);
        Box health = new Box();
        health.setId(2L);
        Box education = new Box();
        education.setId(1L);
        allBoxes.put(health.getId(), health);
        allBoxes.put(education.getId(), education);
        when(boxDAO.findById(eq(1L),eq(datePeriod))).thenReturn(education);
        final Repository<Box> boxRepository = BoxRepository.newInstance(boxDAO);
        Assert.assertTrue(((BoxRepository)boxRepository).findById(1L, datePeriod).equals(allBoxes.get(1L)));
        Assert.assertTrue(((BoxRepository)boxRepository).findById(1L, new DatePeriod("1/1/2015", "31/1/2015")) == null);
        Assert.assertTrue(((BoxRepository)boxRepository).findById(3L, datePeriod) == null);
    }

    @Test
    public void findByDescription(){
        final Map<Long, Box> allBoxes = new HashMap<Long, Box>(0);
        Box health = new Box();
        health.setDescription("Health");
        health.setId(2L);
        Box education = new Box();
        education.setDescription("Education");
        education.setId(1L);
        allBoxes.put(health.getId(), health);
        allBoxes.put(education.getId(), education);
        when(boxDAO.findByDescription(education.getDescription())).thenReturn(education);
        final BoxRepository boxRepository = BoxRepository.newInstance(boxDAO);
        Assert.assertTrue(boxRepository.findByDescription(education.getDescription()).equals(allBoxes.get(1L)));
        Assert.assertTrue(boxRepository.findByDescription("Leisure") == null);
    }

    @Test
    public void findAll_byDatePeriod() throws ParseException {
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Collection<Box> allBoxes = new ArrayList<Box>(0);
        final Box health = new Box();
        health.setId(2L);
        final Box education = new Box();
        education.setId(1L);
        allBoxes.add(health);
        allBoxes.add(education);
        when(boxDAO.findAll(eq(datePeriod))).thenReturn(allBoxes);
        final Repository<Box> boxRepository = BoxRepository.newInstance(boxDAO);
        Assert.assertTrue(boxRepository.findAll(datePeriod).equals(allBoxes));
    }

    @Test
    public void delete(){
        final Box healthBox = new Box();
        healthBox.setId(1L);
        when(boxDAO.delete(healthBox)).thenReturn(true);
        final Repository<Box> boxRepository = BoxRepository.newInstance(boxDAO);
        final boolean deleted = boxRepository.delete(healthBox);
        Assert.assertTrue(
                deleted
        );
    }

    @Test
    public void sumOutgoes() throws ParseException {
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(400));
        final Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(400));
        when(boxDAO.sum(EntryType.OUTGO, datePeriod)).thenReturn(healthBox.getOutgoSum().add(educationBox.getOutgoSum()));
        final BoxRepository boxRepository = BoxRepository.newInstance(boxDAO);
        final BigDecimal outcomeSum = boxRepository.sumOutgo(datePeriod);
        Assert.assertTrue(
                outcomeSum.equals(BigDecimal.valueOf(800))
        );
    }

    @Test
    public void sumIncomes() throws ParseException {
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Box healthBox = new Box();
        healthBox.setOutgoSum(BigDecimal.valueOf(400));
        healthBox.setIncomeSum(BigDecimal.valueOf(200));
        final Box educationBox = new Box();
        educationBox.setOutgoSum(BigDecimal.valueOf(400));
        educationBox.setIncomeSum(BigDecimal.valueOf(200));
        when(boxDAO.sum(EntryType.INCOME, datePeriod)).thenReturn(healthBox.getIncomeSum().add(educationBox.getIncomeSum()));
        final BoxRepository boxRepository = BoxRepository.newInstance(boxDAO);
        final BigDecimal outcomeSum = boxRepository.sumIncome(datePeriod);
        Assert.assertTrue(
                outcomeSum.equals(BigDecimal.valueOf(400))
        );
    }
}
