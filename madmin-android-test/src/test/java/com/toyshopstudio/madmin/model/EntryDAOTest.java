package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.*;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.ContentValuesHolder;
import com.toyshopstudio.madmin.infra.data.DataBase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/18/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EntryDAOTest {

    @Mock
    private final DataBase db = Mockito.mock(DataBase.class);
    @Mock
    private final Cursor cursor = Mockito.mock(Cursor.class);
    private EntryDAO entryDAO;

    @Test
    public void save(){
        final Box box = new Box();
        box.setId(1L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.TEN);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry entry = new Outgo();
        entry.setBox(box);
        entry.setValue(BigDecimal.TEN);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(
                db.save(
                        any(String.class),
                        argThat(new ArgumentMatcher<ContentValuesHolder>() {
                            public boolean matches(Object o) {
                                return (null == ((ContentValuesHolder)o).get("_id"));
                            }
                        })
                )
        ).thenReturn(true);
        entryDAO = EntryDAO.newInstance(db);
        final Entry outcome = entryDAO.save(entry);

        Assert.assertTrue(
                outcome.equals(entry)
        );
    }

    @Test
    public void save_anExistingEntry(){
        final Box box = new Box();
        box.setId(1L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.TEN);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry entry = new Outgo();
        entry.setId(3L);
        entry.setBox(box);
        entry.setValue(BigDecimal.TEN);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(db.update(
            any(String.class),
                argThat(new ArgumentMatcher<ContentValuesHolder>() {
                    public boolean matches(Object o) {
                        return (null != ((ContentValuesHolder)o).get("_id")) &&
                                !(Long.valueOf(0L).equals(((ContentValuesHolder)o).get("_id")));
                    }
                }))
        ).thenReturn(true);
        entryDAO = EntryDAO.newInstance(db);
        final Entry outcome = entryDAO.save(entry);

        Assert.assertTrue(
                outcome.equals(entry)
        );
    }

    @Test
    public void save_aMonthlyIncome(){
        final Entry entry = new Income();
        entry.setBox(null);
        entry.setDateCreated(new Date());
        entry.setValue(BigDecimal.TEN);

        when(cursor.moveToNext()).thenReturn(true, false);
        when(
                db.save(
                        any(String.class),
                        argThat(new ArgumentMatcher<ContentValuesHolder>() {
                            public boolean matches(Object o) {
                                return (null == ((ContentValuesHolder)o).get("_id"));
                            }
                        })
                )
        ).thenReturn(true);
        entryDAO = EntryDAO.newInstance(db);
        final Entry outcome = entryDAO.save(entry);

        Assert.assertTrue(
                outcome.equals(entry)
        );
    }

    @Test(expected=NotImplementedException.class)
    public void findById(){
        entryDAO = EntryDAO.newInstance(db);
        entryDAO.findById(1L,null);
    }

    @Test
    public void findAll() throws ParseException {
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(box);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        final Entry vitamins = new Outgo();
        vitamins.setId(5L);
        vitamins.setBox(box);
        vitamins.setDateCreated(new Date());
        vitamins.setValue(BigDecimal.ONE);
        vitamins.setDescription("Centrum");
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(medicines);
        expenses.add(vitamins);

        when(cursor.moveToNext()).thenReturn(true, true, false);
        when(cursor.getInt(0)).thenReturn(EntryType.OUTGO.getNumber());
        when(cursor.getLong(1)).thenReturn(medicines.getId(),vitamins.getId());
        when(cursor.getString(2)).thenReturn(medicines.getDescription(),vitamins.getDescription());
        when(cursor.getDouble(3)).thenReturn(medicines.getValue().doubleValue(),vitamins.getValue().doubleValue());
        when(cursor.getLong(4)).thenReturn(medicines.getBox().getId(),vitamins.getBox().getId());
        when(cursor.getLong(5)).thenReturn(medicines.getDateCreated().getTime(),vitamins.getDateCreated().getTime());
        when(db.findAll(any(String.class),any(String[].class),any(String.class))).thenReturn(cursor);
        entryDAO = EntryDAO.newInstance(db);
        final Collection<Entry> outcome = entryDAO.findAll(new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                ((Entry)expenses.toArray()[0]).getId().equals(((Entry)outcome.toArray()[0]).getId()) &&
                        ((Entry)expenses.toArray()[1]).getId().equals(((Entry)outcome.toArray()[1]).getId())
        );
    }

    @Test
    public void findAllByBox() {
        final Box healthBox = new Box();
        healthBox.setId(3L);
        healthBox.setDescription("Health");
        healthBox.setOutgoSum(BigDecimal.ZERO);
        healthBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        healthBox.setIncomeSum(BigDecimal.ZERO);
        final Box foodBox = new Box();
        foodBox.setId(3L);
        foodBox.setDescription("Food");
        foodBox.setOutgoSum(BigDecimal.ZERO);
        foodBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        foodBox.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(healthBox);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        final Entry croissant = new Outgo();
        croissant.setId(5L);
        croissant.setBox(foodBox);
        croissant.setDateCreated(new Date());
        croissant.setValue(BigDecimal.ONE);
        croissant.setDescription("Croissant");

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getInt(0)).thenReturn(EntryType.OUTGO.getNumber());
        when(cursor.getLong(1)).thenReturn(croissant.getId());
        when(cursor.getString(2)).thenReturn(croissant.getDescription());
        when(cursor.getDouble(3)).thenReturn(croissant.getValue().doubleValue());
        when(cursor.getLong(4)).thenReturn(croissant.getBox().getId());
        when(cursor.getLong(5)).thenReturn(croissant.getDateCreated().getTime());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        entryDAO = EntryDAO.newInstance(db);
        final Collection<Entry> outcome = entryDAO.findAllByBox(foodBox);

        Assert.assertTrue(
                croissant.getId().equals(((Entry)outcome.toArray()[0]).getId())
        );
    }

    @Test
    public void findAllBy() throws ParseException {
        final Box healthBox = new Box();
        healthBox.setId(3L);
        healthBox.setDescription("Health");
        healthBox.setOutgoSum(BigDecimal.ZERO);
        healthBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        healthBox.setIncomeSum(BigDecimal.ZERO);
        final Box foodBox = new Box();
        foodBox.setId(3L);
        foodBox.setDescription("Food");
        foodBox.setOutgoSum(BigDecimal.ZERO);
        foodBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        foodBox.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(healthBox);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        final Entry croissant = new Outgo();
        croissant.setId(5L);
        croissant.setBox(foodBox);
        croissant.setDateCreated(new Date());
        croissant.setValue(BigDecimal.ONE);
        croissant.setDescription("Croissant");

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getInt(0)).thenReturn(EntryType.OUTGO.getNumber());
        when(cursor.getLong(1)).thenReturn(medicines.getId());
        when(cursor.getString(2)).thenReturn(medicines.getDescription());
        when(cursor.getDouble(3)).thenReturn(medicines.getValue().doubleValue());
        when(cursor.getLong(4)).thenReturn(medicines.getBox().getId());
        when(cursor.getLong(5)).thenReturn(medicines.getDateCreated().getTime());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        entryDAO = EntryDAO.newInstance(db);
        final Collection<Entry> outcome = entryDAO.findAllBy(healthBox, new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                medicines.getId().equals(((Entry)outcome.toArray()[0]).getId())
        );
    }

    @Test
    public void findAllIncome() throws ParseException {
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(box);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        final Entry friendsMedicines = new Income();
        friendsMedicines.setId(5L);
        friendsMedicines.setBox(box);
        friendsMedicines.setDateCreated(new Date());
        friendsMedicines.setValue(BigDecimal.ONE);
        friendsMedicines.setDescription("Centrum payback");

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getInt(0)).thenReturn(EntryType.INCOME.getNumber());
        when(cursor.getLong(1)).thenReturn(friendsMedicines.getId());
        when(cursor.getString(2)).thenReturn(friendsMedicines.getDescription());
        when(cursor.getDouble(3)).thenReturn(friendsMedicines.getValue().doubleValue());
        when(cursor.getLong(4)).thenReturn(friendsMedicines.getBox().getId());
        when(cursor.getLong(5)).thenReturn(friendsMedicines.getDateCreated().getTime());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        entryDAO = EntryDAO.newInstance(db);
        final Collection<Entry> outcome = entryDAO.findAllIncome(new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                friendsMedicines.getId().equals(((Entry)outcome.toArray()[0]).getId())
        );
    }

    @Test(expected=EntryTypeException.class)
    public void createEntry_withoutEntryType() throws ParseException {
        final Box foodBox = new Box();
        foodBox.setId(3L);
        foodBox.setDescription("Food");
        foodBox.setOutgoSum(BigDecimal.ZERO);
        foodBox.setOutgoMonthlyAverage(BigDecimal.ZERO);
        foodBox.setIncomeSum(BigDecimal.ZERO);

        final Entry croissant = new Outgo();
        croissant.setId(5L);
        croissant.setBox(foodBox);
        croissant.setDateCreated(new Date());
        croissant.setValue(BigDecimal.ONE);
        croissant.setDescription("Croissant");

        when(cursor.moveToNext()).thenReturn(true, false);
        when(cursor.getInt(0)).thenAnswer(RETURNS_SMART_NULLS);
        when(cursor.getLong(1)).thenReturn(croissant.getId());
        when(cursor.getString(2)).thenReturn(croissant.getDescription());
        when(cursor.getDouble(3)).thenReturn(croissant.getValue().doubleValue());
        when(cursor.getLong(4)).thenReturn(croissant.getBox().getId());
        when(cursor.getLong(5)).thenReturn(croissant.getDateCreated().getTime());
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        entryDAO = EntryDAO.newInstance(db);
        entryDAO.findAllBy(croissant, new DatePeriod("1/1/2016","31/12/2016"));
    }

    @Test
    public void delete() {
        final Box healthBox = new Box();
        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(healthBox);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        when(db.delete(any(String.class),eq(medicines.getId()))).thenReturn(true);
        entryDAO = EntryDAO.newInstance(db);
        final boolean outcome = entryDAO.delete(medicines);
        Assert.assertTrue(
                outcome
        );
    }

    @Test
    public void sum() throws ParseException {
        final Box box = new Box();
        box.setId(3L);
        box.setDescription("Health");
        box.setOutgoSum(BigDecimal.ZERO);
        box.setOutgoMonthlyAverage(BigDecimal.ZERO);
        box.setIncomeSum(BigDecimal.ZERO);

        final Entry medicines = new Outgo();
        medicines.setId(4L);
        medicines.setBox(box);
        medicines.setDateCreated(new Date());
        medicines.setValue(BigDecimal.ONE);
        medicines.setDescription("Dramin");
        final Entry vitamins = new Outgo();
        vitamins.setId(5L);
        vitamins.setBox(box);
        vitamins.setDateCreated(new Date());
        vitamins.setValue(BigDecimal.ONE);
        vitamins.setDescription("Centrum");
        when(cursor.moveToNext()).thenReturn(true, true, false);
        when(db.executeQuery(any(String.class),any(String[].class))).thenReturn(cursor);
        when(cursor.getDouble(0)).thenReturn(medicines.getValue().add(vitamins.getValue()).doubleValue());
        entryDAO = EntryDAO.newInstance(db);
        final BigDecimal outcome = entryDAO.sum(EntryType.OUTGO, new DatePeriod("1/1/2016","31/12/2016"));

        Assert.assertTrue(
                outcome.compareTo(BigDecimal.valueOf(2)) == 0
        );
    }
}
