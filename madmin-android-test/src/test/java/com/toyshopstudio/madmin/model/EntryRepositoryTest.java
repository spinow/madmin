package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.*;
import com.toyshopstudio.madmin.infra.DatePeriod;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

/**
 *
 * Created by Saulo Junior on 8/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class EntryRepositoryTest {

    @Mock
    private final EntryDAO entryDAO = Mockito.mock(EntryDAO.class);

    @Test(expected=RuntimeException.class)
    public void defaultConstructor(){
        new EntryRepository();
    }

    @Test
    public void save(){
        Entry salary = new Income();
        salary.setId(2L);
        Entry expense = new Outgo();
        expense.setId(1L);

        when(entryDAO.save(argThat(new ArgumentMatcher<Entry>() {
            public boolean matches(Object o) {
                return (null == ((Entry)o).getId()) || ((Entry)o).getId().equals(0L);
            }
        }))).thenReturn(expense);

        final Repository<Entry> entryRepository = EntryRepository.newInstance(entryDAO);
        Assert.assertTrue(entryRepository.save(new Outgo()).getId().equals(1L));
    }

    @Test
    public void findAll(){
        final Entry medicine = new Outgo();
        medicine.setValue(BigDecimal.valueOf(50));
        final Entry book = new Outgo();
        book.setValue(BigDecimal.valueOf(19));
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(medicine);
        expenses.add(book);
        when(entryDAO.findAll()).thenReturn(expenses);
        final Repository<Entry> entryRepository = EntryRepository.newInstance(entryDAO);
        final Collection<Entry> outcomeList = entryRepository.findAll(null);
        Assert.assertTrue(
                outcomeList.containsAll(expenses)
        );
    }

    @Test
    public void findAllByBox() throws ParseException {
        Box educationBox  = new Box();
        final Entry museumTicket = new Outgo();
        museumTicket.setBox(educationBox);
        museumTicket.setValue(BigDecimal.valueOf(5));
        final Entry lunch = new Outgo();
        final Entry breakfast = new Outgo();
        lunch.setBox(new Box());
        lunch.setDescription("McDonald's");
        lunch.setDateCreated(new Date());
        breakfast.setBox(lunch.getBox());
        breakfast.setDescription(lunch.getDescription());
        breakfast.setDateCreated(lunch.getDateCreated());
        lunch.setValue(BigDecimal.valueOf(50));
        breakfast.setValue(BigDecimal.TEN);
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(museumTicket);
        when(entryDAO.findAllBy(Matchers.refEq(educationBox), any(DatePeriod.class))).thenReturn(expenses);
        final Repository<Entry> entryRepository = EntryRepository.newInstance(entryDAO);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        final Collection<Entry> outcomeList = ((EntryRepository)entryRepository).findAllByBox(educationBox, datePeriod);
        Assert.assertTrue(
                outcomeList.contains(museumTicket) && !outcomeList.contains(lunch)
        );
    }

    @Test
    public void findAllIncomes() throws ParseException {
        final Entry medicine = new Outgo();
        medicine.setValue(BigDecimal.valueOf(50));
        final Entry salary = new Income();
        salary.setValue(BigDecimal.valueOf(5000));
        final Collection<Entry> expenses = new ArrayList<Entry>(0);
        expenses.add(medicine);
        final Collection<Entry> incomes = new ArrayList<Entry>(0);
        incomes.add(salary);
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        when(entryDAO.findAllIncome(datePeriod)).thenReturn(incomes);
        final Repository<Entry> entryRepository = EntryRepository.newInstance(entryDAO);
        final Collection<Entry> outcomeList = ((EntryRepository)entryRepository).findAllIncomes(datePeriod);
        Assert.assertTrue(
                !outcomeList.containsAll(expenses) && (outcomeList.containsAll(incomes))
        );
    }

    @Test
    public void delete(){
        final Entry healthBox = new Outgo();
        healthBox.setId(1L);
        when(entryDAO.delete(healthBox)).thenReturn(true);
        final Repository<Entry> entryService = EntryRepository.newInstance(entryDAO);
        final boolean deleted = entryService.delete(healthBox);
        Assert.assertTrue(
                deleted
        );
    }

    @Test
    public void sumIncome() throws ParseException {
        final Entry medicine = new Outgo();
        medicine.setValue(BigDecimal.valueOf(50));
        final Entry salary = new Income();
        salary.setDescription("My salary");
        salary.setValue(BigDecimal.valueOf(5000));
        salary.setDateCreated(new Date());
        final Entry extra = new Income();
        salary.setValue(BigDecimal.valueOf(3000));
        final Collection<Entry> incomes = new ArrayList<Entry>(0);
        incomes.add(salary);
        incomes.add(extra);
        final BigDecimal sum = incomes.iterator().next().getValue().add(incomes.iterator().next().getValue());
        final DatePeriod datePeriod = new DatePeriod("1/1/2016", "31/1/2016");
        when(entryDAO.sum(EntryType.INCOME, datePeriod)).thenReturn(sum);
        final EntryRepository entryRepository = EntryRepository.newInstance(entryDAO);
        final BigDecimal outcome = entryRepository.sumIncome(datePeriod);
        Assert.assertTrue(
                sum.equals(outcome)
        );
    }
}
