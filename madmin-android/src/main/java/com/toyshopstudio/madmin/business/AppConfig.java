package com.toyshopstudio.madmin.business;

/**
 * Domain for the application configurations.
 * Created by Saulo Junior on 7/14/2016.
 */
public class AppConfig extends Domain {

    private Long accessCounter;

    public Long getAccessCounter() {
        return accessCounter;
    }

    public void setAccessCounter(Long accessCounter) {
        this.accessCounter = accessCounter;
    }

    public static String getTableName() {
        return "app_config";
    }

    public void increaseAccessCounter(){
        this.accessCounter++;
    }
}
