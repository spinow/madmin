package com.toyshopstudio.madmin.business;

import com.toyshopstudio.madmin.infra.MadminFormatTool;
import com.toyshopstudio.madmin.infra.ToyshopFormatTool;

import java.math.BigDecimal;

/**
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 21:21
 */
public class Box extends Domain {

    private String description;
    private BigDecimal outgoSum;
    private BigDecimal incomeSum;
    private BigDecimal outgoMonthlyAverage;
    private Float outgoPercentageInPeriod;
    private Integer barSize;

    public Box () {
    }

    public Box (Long id) {
        this.setId(id);
    }

    public String getDescription(){
        return this.description;
    }

    public BigDecimal getOutgoSum() {
        return this.outgoSum;
    }

    public BigDecimal getIncomeSum() {
        return this.incomeSum;
    }

    public String getFormattedOutgoSum(){
        String v = "";
        if(this.outgoSum != null && this.outgoSum.compareTo(BigDecimal.ZERO) > 0){
            v = new MadminFormatTool().formatWithBalanceSymbol(ToyshopFormatTool.formatNumber(this.outgoSum), MadminFormatTool.BalanceSymbol.OUTGO);
        }
        return v;
    }

    public String getFormattedIncomeSum(){
        String v = "";
        if(this.incomeSum != null && this.incomeSum.compareTo(BigDecimal.ZERO) > 0){
            v = new MadminFormatTool().formatWithBalanceSymbol(ToyshopFormatTool.formatNumber(this.incomeSum), MadminFormatTool.BalanceSymbol.INCOME);
        }
        return v;
    }

    public String getFormattedBalance(){
        String v = "";
        if( (this.getIncomeSum() != null && this.getOutgoSum() != null) &&
                (this.getOutgoSum().compareTo(BigDecimal.ZERO) > 0 & this.getIncomeSum().compareTo(BigDecimal.ZERO) > 0)){
            BigDecimal balance = this.getIncomeSum().subtract(this.getOutgoSum());
            String operator = balance.compareTo(BigDecimal.ZERO) > 0 ?
                    MadminFormatTool.BalanceSymbol.POSITIVE.getSymbol() : "";
            v = operator.concat(ToyshopFormatTool.formatNumber(balance));
        }
        return v;
    }

    public String getFormattedMonthlyAverage(){
        String v = "";
        if(this.outgoMonthlyAverage != null && this.outgoMonthlyAverage.compareTo(BigDecimal.ZERO) > 0){
            v = ToyshopFormatTool.formatNumber(this.outgoMonthlyAverage);
        }
        return v;
    }

    public void setOutgoSum(BigDecimal sum) {
        this.outgoSum = sum;
    }

    public void setIncomeSum(BigDecimal sum) {
        this.incomeSum = sum;
    }

    public void setDescription(String description){
        this.description = description;
    }

    /**
     * Number of chars limited text
     * @param max the maximum number of characters to be retuned along with '...' in the end.
     * @return description limited number of characters with '...' in the end.
     */
    public String getDescription(Integer max){
        String description = this.description != null ? this.description : "";
        if (description.length() > max){
            description = description.substring(0, max).concat("...");
        }
        return description;
    }

    public static String getTableName() {
        return "label";
    }

    public BigDecimal getOutgoMonthlyAverage() {
        return outgoMonthlyAverage;
    }

    public void setOutgoMonthlyAverage(BigDecimal outgoMonthlyAverage) {
        this.outgoMonthlyAverage = outgoMonthlyAverage;
    }

    public Float getOutgoPercentageInPeriod() {
        return outgoPercentageInPeriod;
    }

    public void setOutgoPercentageInPeriod(Float outgoPercentageInPeriod) {
        this.outgoPercentageInPeriod = outgoPercentageInPeriod;
    }

    public Integer getBarSize() {
        return barSize;
    }

    public void setBarSize(Integer barSize) {
        this.barSize = barSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Box)) return false;

        Box box = (Box) o;

        return description != null ? description.equals(box.description) : box.description == null;

    }

    @Override
    public int hashCode() {
        return description != null ? description.hashCode() : 0;
    }
}
