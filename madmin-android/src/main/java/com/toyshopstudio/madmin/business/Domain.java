package com.toyshopstudio.madmin.business;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 22:49
 * To change this template use File | Settings | File Templates.
 */
public abstract class Domain {

    private Long id;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }
}
