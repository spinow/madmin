package com.toyshopstudio.madmin.business;

import com.toyshopstudio.madmin.infra.ToyshopFormatTool;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 21:20
 * To change this template use File | Settings | File Templates.
 */
public abstract class Entry extends Domain {

    private BigDecimal value = BigDecimal.valueOf(0);
    private String description;
    private Box box;
    private Date dateCreated;
    EntryType type;

    public EntryType getType() {
        return this.type;
    }

    public BigDecimal getValue(){
        return this.value;
    }

    public String getFormattedValue(){
        String v = "";
        if(this.value != null){
            v = ToyshopFormatTool.formatNumber(this.value);
        }
        return v;
    }

    public String getDescription(){
        return this.description;
    }

    /**
     * Number of chars limited text
     * @param max the maximum number of characters to be retuned along with '...' in the end.
     * @return description limited number of characters with '...' in the end.
     */
    String getDescription(Integer max){
        String description = this.description != null ? this.description : "";
        if (description.length() > max){
            description = description.substring(0, max).concat("...");
        }
        return description;
    }

    public Box getBox(){
        return this.box;
    }

    public Date getDateCreated(){
        return this.dateCreated;
    }

    public void setValue(BigDecimal value){
        this.value = value;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setBox(Box box){
        this.box = box;
    }

    public void setDateCreated(Date dateCreated){
        this.dateCreated = dateCreated;
    }

    public static String getTableName() {
        return "entry";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entry)) return false;

        Entry entry = (Entry) o;

        if (!value.equals(entry.value)) return false;
        if (description != null ? !description.equals(entry.description) : entry.description != null) return false;
        if (box != null ? !box.equals(entry.box) : entry.box != null) return false;
        if (dateCreated != null ? !dateCreated.equals(entry.dateCreated) : entry.dateCreated != null) return false;
        return type == entry.type;

    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (box != null ? box.hashCode() : 0);
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
