package com.toyshopstudio.madmin.business;

/**
 * Used to define a specialization of Entry.
 * Created by Saulo Junior on 16/12/2014.
 */
public enum EntryType {

    OUTGO(1),
    INCOME(2);

    private Integer number;

    EntryType(Integer number){
        this.number = number;
    }

    public Integer getNumber() {
        return this.number;
    }
}
