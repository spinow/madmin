package com.toyshopstudio.madmin.business;

/**
 *
 * Created by Saulo Junior on 8/2/2016.
 */
public class EntryTypeException extends RuntimeException {

    public EntryTypeException() {
        super("Entry type not defined!");
    }

}
