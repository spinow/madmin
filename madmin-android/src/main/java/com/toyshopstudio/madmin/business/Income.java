package com.toyshopstudio.madmin.business;

/**
 * Represents an Entry of the type Income.
 * Created by Saulo Junior on 16/12/2014.
 */
public class Income extends Entry {

    public Income() {
        this.type = EntryType.INCOME;
    }

}
