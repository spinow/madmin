package com.toyshopstudio.madmin.business;

/**
 * Represents an Entry of the type expense.
 * Created by Saulo Junior on 16/12/2014.
 */
public class Outgo extends Entry {

    public Outgo() {
        this.type = EntryType.OUTGO;
    }

}
