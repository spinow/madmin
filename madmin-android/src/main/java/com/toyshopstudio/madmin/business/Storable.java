package com.toyshopstudio.madmin.business;

/**
 *
 * Created by Saulo Junior on 8/19/2016.
 */
public interface Storable {

    public String getTableName();

}
