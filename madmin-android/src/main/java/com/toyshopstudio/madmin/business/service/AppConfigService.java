package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.model.AppConfigRepository;

/**
 * Service for the application settings.
 * Created by Saulo Junior on 7/15/2016.
 */
public class AppConfigService {

    private final AppConfigRepository appConfigRepository;

    AppConfigService(){
        throw new RuntimeException("Cannot invoke default constructor!");
    }

    private AppConfigService(AppConfigRepository appConfigRepository) {
        this.appConfigRepository = appConfigRepository;
    }

    public static AppConfigService newInstance(AppConfigRepository appConfigRepository){
        return new AppConfigService(appConfigRepository);
    }

    public AppConfig getAppConfig(){
        return appConfigRepository.find();
    }

    public void increaseAccessCount(){
        AppConfig appConfig = getAppConfig();
        appConfig.increaseAccessCounter();
        appConfigRepository.save(appConfig);
    }

}
