package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.MadminFormatTool;
import com.toyshopstudio.madmin.infra.ToyshopFormatTool;
import com.toyshopstudio.madmin.model.BoxRepository;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 21:59
 */
public class BoxService {

    private static BoxService boxService;
    private final BoxRepository boxRepository;

    BoxService(){
        throw new RuntimeException("Cannot invoke default constructor!");
    }

    private BoxService(final BoxRepository boxRepository){
        this.boxRepository = boxRepository;
    }

    public static BoxService newInstance(BoxRepository boxRepository){
        boxService = new BoxService(boxRepository);
        return boxService;
    }

    @SuppressWarnings("unchecked")
    public Collection<Box> getAll(final DatePeriod datePeriod){
        final Collection<Box> boxes = boxRepository.findAll(datePeriod);
        for(Box box : boxes) {
            box.setOutgoPercentageInPeriod(boxService.getBoxOutgoPercentageInPeriod(this.getOutgoSum(datePeriod), box));
        }
        return boxes;
    }

    public Box getById(Long id, DatePeriod datePeriod){
        return boxRepository.findById(id, datePeriod);
    }

    Float getBoxOutgoPercentageInPeriod(final BigDecimal periodOutgoSum, final Box box) {
        Float r = 0F;
        if((box.getOutgoSum() != null && box.getOutgoSum().doubleValue() > 0F)
                && (periodOutgoSum != null && periodOutgoSum.doubleValue() > 0F)) {
            r = ((box.getOutgoSum().floatValue() / periodOutgoSum.floatValue() )) * 100;
        }
        return r;
    }

    public Boolean delete(Box box){
        return boxRepository.delete(box);
    }

    public Box save(Box box) {
        return boxRepository.save(box);
    }

    private BigDecimal getOutgoSum(DatePeriod datePeriod) {
        return boxRepository.sumOutgo(datePeriod);
    }

    public BigDecimal getOutgoSum(Collection<Box> boxes) {
        BigDecimal sum = new BigDecimal(0);
        for (Box box : boxes) {
            sum = sum.add(box.getOutgoSum());
        }
        return sum;
    }

    public String getFormattedOutgoSum(BigDecimal outgoSum, DatePeriod datePeriod) {
        String v = "";
        outgoSum = outgoSum == null ? this.getOutgoSum(datePeriod) : outgoSum;
        if(outgoSum.compareTo(BigDecimal.ZERO) > 0){
            v = new MadminFormatTool().formatWithBalanceSymbol(
                    ToyshopFormatTool.formatNumber(outgoSum), MadminFormatTool.BalanceSymbol.OUTGO
            );
        }
        return v;
    }

    public Box getByDescription(String description){
        return boxRepository.findByDescription(description);
    }
}
