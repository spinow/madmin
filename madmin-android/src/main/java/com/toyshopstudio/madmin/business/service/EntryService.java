package com.toyshopstudio.madmin.business.service;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.MadminFormatTool;
import com.toyshopstudio.madmin.infra.ToyshopFormatTool;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.model.Repository;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 22:15
 */
public class EntryService {

    private EntryRepository entryRepository;

    EntryService(){
        throw new RuntimeException("Cannot invoke default constructor!");
    }

    private EntryService(EntryRepository entryRepository){
        this.entryRepository = entryRepository;
    }

    public static EntryService newInstance(EntryRepository entryRepository){
        return new EntryService(entryRepository);
    }

    /**
     * @param entries to be summarized
     * @return incomes sum from an entry list.
     */
    public BigDecimal sumIncomes (Collection<Entry> entries) {
        BigDecimal sum = new BigDecimal(0);
        for (Entry entry : entries) {
            if (entry.getType().equals(EntryType.INCOME)) {
                sum = sum.add(entry.getValue());
            }
        }
        return sum;
    }

    public Collection<Entry> getAllByBox(Box box, DatePeriod datePeriod){
        return entryRepository.findAllByBox(box, datePeriod);
    }

    public Collection<Entry> getAllIncomes(DatePeriod datePeriod){
        return entryRepository.findAllIncomes(datePeriod);
    }

    public Boolean delete(Entry entry){
        return entryRepository.delete(entry);
    }

    public Entry save(Entry entry) {
        return entryRepository.save(entry);
    }

    public BigDecimal getSelectedPeriodIncomeSum(DatePeriod datePeriod) {
        return entryRepository.sumIncome(datePeriod);
    }

    public String getFormattedSelectedPeriodIncomeSum(DatePeriod datePeriod) {
        return getFormattedSelectedPeriodIncomeSum(null, datePeriod);
    }

    public String getFormattedSelectedPeriodIncomeSum(BigDecimal incomeSum, DatePeriod datePeriod) {
        String v = "";
        incomeSum = incomeSum == null ? this.getSelectedPeriodIncomeSum(datePeriod) : incomeSum;
        if(incomeSum != null && incomeSum.compareTo(BigDecimal.ZERO) > 0){
            v = new MadminFormatTool().formatWithBalanceSymbol(ToyshopFormatTool.formatNumber(incomeSum), MadminFormatTool.BalanceSymbol.INCOME);
        }
        return v;
    }

    public String getFormattedSelectedPeriodBalance(BigDecimal incomeSum, BigDecimal outgoSum) {
        String v = "";
        if((incomeSum != null && incomeSum.compareTo(BigDecimal.ZERO) > 0) && (outgoSum != null && outgoSum.compareTo(BigDecimal.ZERO) > 0)){
            BigDecimal balance = incomeSum.subtract(outgoSum);
            String operator = balance.compareTo(BigDecimal.ZERO) > 0 ? "+ " : "";
            v = operator.concat(ToyshopFormatTool.formatNumber(balance));
        }
        return v;
    }

}
