package com.toyshopstudio.madmin.infra;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.util.Patterns;

import java.util.regex.Pattern;

/**+
 * Created by Saulo Junior on 06/10/2015.
 */
public class AccountsTool {

    public static String getAccountEmail (final AccountManager accountManager) {
        String email = null;
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                email = account.name;
            }
        }
        return email;
    }

}
