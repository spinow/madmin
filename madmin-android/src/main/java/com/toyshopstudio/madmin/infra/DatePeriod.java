package com.toyshopstudio.madmin.infra;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 16/09/14
 * Time: 00:03
 * To change this template use File | Settings | File Templates.
 */
public class DatePeriod implements Serializable {

    private Date initialDate;
    private Date finalDate;

    public DatePeriod (final Date initialDate, final Date finalDate) {
        this.initialDate = initialDate;
        this.finalDate = finalDate;
    }

    public DatePeriod (final String initialDate, final String finalDate) throws ParseException {
            this.initialDate = new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(initialDate);
            this.finalDate = new SimpleDateFormat("dd/MM/yyyy", Locale.UK).parse(finalDate);
            new DatePeriod(this.initialDate, this.finalDate);
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public static DatePeriod getSelectedPeriod(DatePeriod datePeriod){
        if (datePeriod == null) {
            Date initialDate = new Date();
            Date finalDate = initialDate;
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(initialDate);
            calendar.set(GregorianCalendar.DAY_OF_MONTH, calendar.getActualMinimum(GregorianCalendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            initialDate = calendar.getTime();
            calendar.setTime(finalDate);
            calendar.set(GregorianCalendar.DAY_OF_MONTH, calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            finalDate = calendar.getTime();
            datePeriod = new DatePeriod(initialDate, finalDate);
        }
        return datePeriod;
    }

    /**
     * Indicates whenever the selected date period is within the actual period (month).
     * @param selectedDatePeriod
     * @return
     */
    public static Boolean isSelectedPeriodCurrent(DatePeriod selectedDatePeriod) {
        if (null != selectedDatePeriod) {
            DatePeriod currentDatePeriod = getSelectedPeriod(null);
            return  currentDatePeriod.getInitialDate().getTime() >= selectedDatePeriod.getInitialDate().getTime() &&
                    currentDatePeriod.getFinalDate().getTime() <= selectedDatePeriod.getFinalDate().getTime();
        }
        return true;
    }

}
