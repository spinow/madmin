package com.toyshopstudio.madmin.infra;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * User: Saulo Junior
 * Date: 21/09/14
 * Time: 00:53
 */
public class MadminFormatTool {

    public MadminFormatTool() {
    }

    public enum BalanceSymbol {
        INCOME("▲ "),
        OUTGO("▼ "),
        POSITIVE("+ ");

        private String symbol;

        BalanceSymbol(String symbol){
            this.symbol = symbol;
        }

        public String getSymbol() {
            return this.symbol;
        }
    }

    private static NumberFormat numberFormatter = new DecimalFormat("#,##0.00");

    public static String formatNumber(BigDecimal number){
        return numberFormatter.format(number);
    }

    public String formatWithBalanceSymbol(String stringValue, BalanceSymbol symbol) {
        return symbol.getSymbol().concat(stringValue);
    }
}
