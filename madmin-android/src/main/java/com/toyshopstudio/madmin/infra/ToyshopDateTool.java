package com.toyshopstudio.madmin.infra;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * User: Saulo Junior
 * Date: 25/02/2015
 * Time: 19:49
 */
public class ToyshopDateTool {

    public static Integer getDateMonthField(Date date) {
        return getDateField(date, GregorianCalendar.MONTH);
    }

    public static Integer getDateYearField(Date date) {
        return getDateField(date, GregorianCalendar.YEAR);
    }

    private static Integer getDateField(Date date, int field) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        return calendar.get(field);
    }
}
