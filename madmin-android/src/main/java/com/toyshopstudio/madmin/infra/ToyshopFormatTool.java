package com.toyshopstudio.madmin.infra;

import android.content.Context;
import com.toyshopstudio.madmin.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * User: Saulo Junior
 * Date: 21/09/14
 * Time: 00:53
 */
public class ToyshopFormatTool {

    private static NumberFormat numberFormatter = new DecimalFormat("#,##0.00");

    public static String formatNumber(BigDecimal number){
        return numberFormatter.format(number);
    }

    public static String formattedDateYear(Date date) {
        return String.valueOf(ToyshopDateTool.getDateYearField(date));
    }

    public static String formattedDateMonth(Context context, Date date) {
        String[] months = context.getResources().getStringArray(R.array.months_array);
        String month = months[ToyshopDateTool.getDateMonthField(date)];
        return month;
    }
}
