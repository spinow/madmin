package com.toyshopstudio.madmin.infra;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.toyshopstudio.madmin.infra.data.DataBaseChangeSet;
import com.toyshopstudio.madmin.infra.data.DataBaseHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 22/08/14
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
public class ToyshopSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final int VERSION = 3;

    public ToyshopSQLiteOpenHelper(Context context){
        super(context, "madmin", null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL(DataBaseChangeSet.BOX_QUERY_CREATE_TABLE);
            sqLiteDatabase.execSQL(DataBaseChangeSet.ENTRY_QUERY_CREATE_TABLE);
            applyChangeSet(sqLiteDatabase,1,VERSION);
        } catch (SQLException e){
            System.out.print(e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        applyChangeSet(sqLiteDatabase, oldVersion, newVersion);
    }

    private void applyChangeSet(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        try{
            sqLiteDatabase.execSQL(DataBaseChangeSet.QUERY_CREATE_TABLE);
            DataBaseHelper.applyChanges(sqLiteDatabase, oldVersion, newVersion);
        } catch (SQLException e){
            System.out.print(e.getMessage());
        }
    }
}
