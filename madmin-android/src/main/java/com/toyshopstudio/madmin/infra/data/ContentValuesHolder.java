package com.toyshopstudio.madmin.infra.data;

import android.content.ContentValues;

import java.util.HashMap;

/**
 * Encapsulates {@link ContentValues} for test purposes.
 * Created by Saulo Junior on 8/16/2016.
 */
public class ContentValuesHolder extends HashMap<String, Object> {

    public ContentValues toContentValues(){
        ContentValues contentValues = new ContentValues();
        for (String key : this.keySet()) {
            Object o = this.get(key);
            if (o instanceof Boolean) {
                contentValues.put(key,((Boolean)this.get(key)));
            } else if (o instanceof String) {
                contentValues.put(key,((String)this.get(key)));
            } else if (o instanceof Integer) {
                contentValues.put(key,((Integer) this.get(key)));
            } else if (o instanceof Long) {
                contentValues.put(key,((Long)this.get(key)));
            } else if (o instanceof Float) {
                contentValues.put(key,((Float) this.get(key)));
            } else if (o instanceof Byte) {
                contentValues.put(key,((Byte)this.get(key)));
            } else if (o instanceof Short) {
                contentValues.put(key,((Short) this.get(key)));
            } else if (o instanceof Double) {
                contentValues.put(key,((Double) this.get(key)));
            }
        }
        return contentValues;
    }

}
