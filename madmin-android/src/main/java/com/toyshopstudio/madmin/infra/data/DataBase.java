package com.toyshopstudio.madmin.infra.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 21:43
 * To change this template use File | Settings | File Templates.
 */
public interface DataBase {

    public DataBase open() throws SQLException;

    public void close();

    public Boolean save(String tableName, ContentValuesHolder fields);

    public Boolean update(String tableName, ContentValuesHolder fields);

    public Boolean delete(String tableName, Long id);

    public Cursor findAll(String tableName, String[] fields, String orderBy);

    public Cursor findById(String tableName, Long id);

    public Cursor executeQuery(String query, String[] params);

    public void execStringSql(String query);

    public void execStringSql(String query, Object[] args);
}
