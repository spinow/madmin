package com.toyshopstudio.madmin.infra.data;

import android.util.SparseArray;
import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Responsible for database changes and change tracking.
 * Created by Saulo Junior on 30/11/2014.
 */
public class DataBaseChangeSet {

    public static String QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS database_changeset ( version INTEGER PRIMARY KEY );";
    static String QUERY_INSERT_VERSION = "INSERT INTO database_changeset (version) VALUES (?);";
    static String QUERY_SELECT_VERSION = "SELECT version FROM database_changeset WHERE version = ?;";

    public static String BOX_QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + Box.getTableName() + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT UNIQUE NOT NULL );";
    public static String ENTRY_QUERY_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + Entry.getTableName() + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT NOT NULL, value REAL NOT NULL, label_id INTEGER, date_created INTEGER, FOREIGN KEY (label_id) REFERENCES box (_id) );";

    private Long version;

    /* v<n> represents the database version (not the app version): */
    private static List<String> v2 = new ArrayList<String>(0);
    private static List<String> v3 = new ArrayList<String>(0);

    private static SparseArray<List<String>> CHANGELOG = new SparseArray<List<String>>(0);

    static List<String> getChangeset(int version) {
        if (CHANGELOG.size() < 1) {
            {
                v2.add("ALTER TABLE " + Entry.getTableName() + " ADD COLUMN type INTEGER;");
                v2.add("UPDATE " + Entry.getTableName() + " SET type = 1;");
                CHANGELOG.put(2, v2);
                v3.add("CREATE TABLE IF NOT EXISTS " + AppConfig.getTableName() + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, access_counter INTEGER );");
                v3.add("INSERT INTO " + AppConfig.getTableName() + "( _id, access_counter ) VALUES ( 1, 0 );");
                CHANGELOG.put(3, v3);
            }
        }
        return DataBaseChangeSet.CHANGELOG.get(version);
    }
}
