package com.toyshopstudio.madmin.infra.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.service.BoxService;
import com.toyshopstudio.madmin.model.BoxDAO;
import com.toyshopstudio.madmin.model.BoxRepository;
import com.toyshopstudio.madmin.model.EntryDAO;

import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 22/08/14
 * Time: 23:38
 */
public class DataBaseHelper {

    public static Collection<Box> insertDefaultBoxData(Context context){
        DataBase dataBase = SQLiteDataBaseImpl.getInstance();
        EntryDAO entryDAO = EntryDAO.newInstance(dataBase);
        BoxService boxService = BoxService.newInstance(
                BoxRepository.newInstance(
                        BoxDAO.newInstance(dataBase, entryDAO)
                )
        );
        Collection<Box> boxes = new ArrayList<Box>(0);
        for(String descricao : context.getResources().getStringArray(R.array.default_boxes)){
            Box box = new Box();
            box.setDescription(descricao);
            box = boxService.save(box);
            boxes.add(box);
        }
        return boxes;
    }

    public static void applyChanges(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion){
        DataBase db = SQLiteDataBaseImpl.getInstance();
        if(!sqLiteDatabase.isOpen()) {
            db.open();
        }
        while(oldVersion <= newVersion) {
            Cursor cursor = sqLiteDatabase.rawQuery(DataBaseChangeSet.QUERY_SELECT_VERSION, new String[]{String.valueOf(oldVersion)});
            if (!cursor.moveToNext()) {
                Collection<String> changeset = DataBaseChangeSet.getChangeset(oldVersion);
                if (changeset != null){
                    for(String query : changeset) {
                        sqLiteDatabase.execSQL(query);
                    }
                    sqLiteDatabase.execSQL(DataBaseChangeSet.QUERY_INSERT_VERSION, new String[]{String.valueOf(oldVersion)});
                }
            }
            oldVersion++;
        }
    }
}
