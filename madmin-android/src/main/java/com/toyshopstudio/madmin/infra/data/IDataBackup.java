package com.toyshopstudio.madmin.infra.data;

import android.content.Context;

import java.io.InputStream;

/**
 * Created by Saulo Junior on 09/05/2015.
 */
public interface IDataBackup {

    public String generateBackup(Context context);

    public Boolean recoverBackup(Context context, InputStream file);

}
