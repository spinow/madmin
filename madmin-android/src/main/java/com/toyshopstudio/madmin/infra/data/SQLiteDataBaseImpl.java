package com.toyshopstudio.madmin.infra.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.toyshopstudio.madmin.infra.ToyshopSQLiteOpenHelper;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 21:43
 * To change this template use File | Settings | File Templates.
 */
public class SQLiteDataBaseImpl implements DataBase {

    private static SQLiteDataBaseImpl instance;
    private ToyshopSQLiteOpenHelper openHelper;
    private SQLiteDatabase db;

    public static SQLiteDataBaseImpl getInstance(){
        if(instance == null){
            instance = new SQLiteDataBaseImpl(ToyshopActivity.getContext());
        }
        return instance;
    }

    private SQLiteDataBaseImpl(Context context){
        this.openHelper = new ToyshopSQLiteOpenHelper(context);
    }

    public SQLiteDataBaseImpl open() throws SQLException {
        db = openHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        try {
            openHelper.close();
        } catch (Exception e) {
            System.out.print("Error trying to close database!");
        }
    }

    @Override
    public Boolean save(String tableName, ContentValuesHolder fields){
        return db.insert(tableName, null, fields.toContentValues()) > 0;
    }

    @Override
    public Boolean update(String tableName, ContentValuesHolder fields) {
        return db.update(tableName, fields.toContentValues(), " _id = " + fields.get("_id"), null) > 0;
    }

    @Override
    public Boolean delete(String tableName, Long id){
        return db.delete(tableName, " _id = " + String.valueOf(id.longValue()), null) > 0;
    }

    @Override
    public Cursor findAll(String tableName, String[] fields, String orderBy) {
        return db.query(tableName, fields, null, null, null, null, orderBy);
    }

    @Override
    public Cursor executeQuery(String query, String[] params) {
        return db.rawQuery(query, params);
    }

    @Override
    public void execStringSql(String query) {
        db.execSQL(query);
    }

    @Override
    public void execStringSql(String query, Object[] args) {
        db.execSQL(query, args);
    }

    @Override
    public Cursor findById(String tableName, Long id) {
        return db.rawQuery("SELECT * FROM " + tableName + " WHERE _id = ?", new String[]{id.toString()});
    }
}
