package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/28/15
 * Time: 5:55 PM
 */
public abstract class BackupData {

    public abstract String backup ();

    public abstract void undoBackup ();

    public abstract String restore ();

    public abstract void undoRestore();

    public abstract void validate() throws BackupDataException;

    public abstract String store (IBackupStorer backupStorer);

    public abstract void undoStore ();

}
