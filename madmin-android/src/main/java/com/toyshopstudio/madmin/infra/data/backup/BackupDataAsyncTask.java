package com.toyshopstudio.madmin.infra.data.backup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.activity.base.ToyshopListActivity;

import java.util.List;
import java.util.Set;

/**
 * Created by Saulo Junior on 26/09/2015.
 */
public class BackupDataAsyncTask extends AsyncTask<Void, Void, String> {

    private Context context;
    private List<DataSecurityEvent> dataSecurityEvents;

    private BackupDataAsyncTask () {}

    public BackupDataAsyncTask(Context context, List<DataSecurityEvent> dataSecurityEvents) {
        this.context = context;
        this.dataSecurityEvents = dataSecurityEvents;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... p) {
        String result = "";
        try {
            DataSecurityInvoker invoker = new DataSecurityInvoker();
            for (DataSecurityEvent dataSecurityEvent : dataSecurityEvents) {
                result = invoker.requestEvent(dataSecurityEvent);//Armazena sempre a última mensagem.
            }
            return result;
        } catch (Exception e) {
            ToastOnMiddle.makeText(context,
                    context.getString(context.getResources().getIdentifier(e.getMessage(),
                            ToyshopListActivity.EXCEPTIONS_BUNDLE_RES, context.getPackageName())),
                    Integer.parseInt(context.getString(R.string.app_toast_time))).show();
            return result;
        }
    }

    @Override
    protected void onPostExecute(String backup) {
        if (backup != null) {
            ToastOnMiddle.makeText(context, backup, Integer.parseInt(context.getString(R.string.app_toast_time))).show();
        }
    }

}
