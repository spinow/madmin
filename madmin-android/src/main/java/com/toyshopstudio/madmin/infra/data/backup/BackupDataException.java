package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/28/15
 * Time: 6:16 PM
 */
public class BackupDataException extends RuntimeException {

    public BackupDataException(String message) {
        super(message);
    }

}
