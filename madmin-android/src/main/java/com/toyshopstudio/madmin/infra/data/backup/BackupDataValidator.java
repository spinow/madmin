package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/28/15
 * Time: 6:28 PM
 */
public interface BackupDataValidator {

    public void validate() throws BackupDataException;

}
