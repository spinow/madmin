package com.toyshopstudio.madmin.infra.data.backup;

import java.io.InputStream;

/**
 * Represents a data file to be backed up or restored.
 * @author Saulo Júnior
 */
public class BackupFile {

    public static final String BACKUP_EXTENSION = ".mad";

    private InputStream file;
    private String path;

    public BackupFile (InputStream file, String path) {
        this.file = file;
        this.path = path;
    }

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
