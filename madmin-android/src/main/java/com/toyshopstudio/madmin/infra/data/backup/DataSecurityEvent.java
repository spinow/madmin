package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/22/15
 * Time: 6:46 PM
 */
public interface DataSecurityEvent {

    public String execute();

    public void undo();

}
