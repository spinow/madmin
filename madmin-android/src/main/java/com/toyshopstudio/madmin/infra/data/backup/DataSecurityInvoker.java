package com.toyshopstudio.madmin.infra.data.backup;

import android.util.SparseArray;

/**
 * Creates an execution queue of DataSecurityEvents.
 */
public class DataSecurityInvoker {

    private int priority = 1;
    private SparseArray<DataSecurityEvent> events = new SparseArray<DataSecurityEvent>(0);

    public String requestEvent(DataSecurityEvent dataSecurityEvent) {
        events.put(priority, dataSecurityEvent);
        try {
            return dataSecurityEvent.execute();
        } catch (Exception e) {
            for ( int i = (events.size() -1); i > 0; i-- ) {
                events.get(i).undo();
            }
            return e.getMessage();
        }
    }

}
