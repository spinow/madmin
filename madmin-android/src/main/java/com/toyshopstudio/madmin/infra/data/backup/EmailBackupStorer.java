package com.toyshopstudio.madmin.infra.data.backup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.toyshopstudio.madmin.view.intent.EmailSender;
import com.toyshopstudio.madmin.view.intent.EmailSenderException;

/**
 * User: Saulo Júnior
 * Date: 9/30/15
 * Time: 6:36 PM
 */
public class EmailBackupStorer implements IBackupStorer {

    private EmailSender emailSender;

    private EmailBackupStorer () {}

    public EmailBackupStorer (EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public String store() throws BackupDataException{
        try {
            return this.emailSender.send();
        } catch (EmailSenderException e) {
            throw new BackupDataException(e.getMessage());
        }
    }
}
