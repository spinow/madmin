package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/30/15
 * Time: 6:36 PM
 */
public interface IBackupStorer {

    public String store();

}
