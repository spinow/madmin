package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/22/15
 * Time: 6:20 PM
 */
public class SQLiteBackup implements IBackup {

    private final BackupData backupData;

    public SQLiteBackup(final BackupData backupData) {
        this.backupData = backupData;
    }

    @Override
    public String execute() {
        return backupData.backup();
    }

    @Override
    public void undo() {
        this.backupData.undoBackup();
    }
}
