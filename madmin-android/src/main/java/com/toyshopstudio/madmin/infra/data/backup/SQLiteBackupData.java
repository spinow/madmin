package com.toyshopstudio.madmin.infra.data.backup;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.view.ToastOnMiddle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by Saulo Junior on 25/09/2015.
 */
public class SQLiteBackupData extends BackupData {

    public static final String TAG = "SQLiteBackupData";

    private Context context;
    private BackupFile file;
    private BackupDataValidator backupDataValidator;

    private FileChannel src;
    private FileChannel dst;

    private SQLiteBackupData () {}

    public SQLiteBackupData(final Context context) {
        this.context = context;
        this.file = new BackupFile(null, null);
    }

    public SQLiteBackupData(final Context context, final BackupFile file) {
        this.file = file;
        new SQLiteBackupData(context, file, new SQLiteBackupDataValidator(this.file));
    }

    public SQLiteBackupData(final Context context, final BackupFile file, final BackupDataValidator backupDataValidator) {
        this.context = context;
        this.file = file;
        this.backupDataValidator = backupDataValidator;
    }

    public BackupFile getFile() {
        return file;
    }

    @Override
    public String backup (){
        String backupFilePath = "";
        try {
            final String appName = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString().toLowerCase();
            final String inFileName = context.getDatabasePath(appName).getAbsolutePath();

            File sd = Environment.getExternalStorageDirectory();
            if (sd.canWrite()) {
                String backupDBPath = appName.concat(BackupFile.BACKUP_EXTENSION);
                File currentDB = new File(inFileName);
                File backupDB = new File(sd, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();
                exchangeFiles(src, dst);

                backupFilePath = backupDB.getPath();
            }
        } catch (IOException ioe) {
            Log.e(TAG, context.getString(R.string.exception_backup_file_io));
        } catch (Exception e) {
            Log.e(TAG, context.getString(R.string.exception_backup_file_io));
        } finally {
            if(!"".equals(backupFilePath)) {
                //Sets the path to be used by the incoming (if any) event responsible for storing the file (i.e. email sender)
                this.file = new BackupFile(null, backupFilePath);
                return context.getString(R.string.success_backup_created);
            } else {
                return context.getString(R.string.exception_backup_file_io);
            }
        }
    }

    @Override
    public void undoBackup() {
        Log.i(TAG,"Undoing backup operation...");
        try {
            exchangeFiles(dst, src);
        } catch (Exception e) {
            Log.e(TAG, "exception_backup_undo");
        }
        Log.i(TAG,"Backup operation successfully done!");
    }

    @Override
    public String restore (){

        Boolean success = false;

        try {
            this.validate();

            final String appName = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString().toLowerCase();
            final String outFilePath = context.getDatabasePath(appName).getAbsolutePath();

            File currentDB = new File(outFilePath);

            src = ((FileInputStream)file.getFile()).getChannel();
            dst = new FileOutputStream(currentDB).getChannel();
            exchangeFiles(src, dst);

            success = true;

        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (BackupDataException bde) {
            Log.e(TAG, bde.getMessage());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (success.equals(true)) {
//                ToastOnMiddle.makeText(context, context.getString(R.string.success_backup_recovered), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                return context.getString(R.string.success_backup_recovered);
            } else {
                return context.getString(R.string.recover_fail);
//                ToastOnMiddle.makeText(context, context.getString(R.string.recover_fail), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
            }
        }
    }

    private void exchangeFiles(FileChannel src, FileChannel dst) throws IOException {
        dst.transferFrom(src, 0, src.size());
        src.close();
        dst.close();
    }

    @Override
    public void undoRestore() {
        Log.i(TAG,"Undoing restore not implemented");
    }

    @Override
    public void validate() throws BackupDataException {
        this.backupDataValidator.validate();
    }

    @Override
    public String store(IBackupStorer backupStorer) {
        return backupStorer.store();
    }

    @Override
    public void undoStore() {
        Log.i(TAG,"Undoing store not implemented");
    }

}
