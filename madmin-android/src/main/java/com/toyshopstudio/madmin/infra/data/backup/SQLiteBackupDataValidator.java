package com.toyshopstudio.madmin.infra.data.backup;

import android.database.sqlite.SQLiteDatabase;
import com.toyshopstudio.madmin.infra.ToyshopSQLiteOpenHelper;

/**
 * User: Saulo Júnior
 * Date: 9/28/15
 * Time: 6:29 PM
 */
public class SQLiteBackupDataValidator implements BackupDataValidator {

    private BackupFile file;

    private SQLiteBackupDataValidator () { }

    public SQLiteBackupDataValidator (final BackupFile file) {
        if (file == null) {
            throw new BackupDataException("exception_backup_restore_file_null");
        }
        this.file = file;
    }

    @Override
    public void validate() throws BackupDataException {
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(file.getPath(), null, SQLiteDatabase.OPEN_READONLY);
            int version = db.getVersion();
            if (ToyshopSQLiteOpenHelper.VERSION != version) {
                throw new BackupDataException("exception.backup.file.version");
            }
        } catch (Exception e) {
            throw new BackupDataException("exception_backup_restore_file_unhandled");
        }
    }
}
