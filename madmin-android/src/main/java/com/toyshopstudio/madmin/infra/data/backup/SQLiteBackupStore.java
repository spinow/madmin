package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/30/15
 * Time: 6:06 PM
 */
public class SQLiteBackupStore implements IBackupStore {

    private final BackupData backupData;
    private final IBackupStorer backupStorer;

    private SQLiteBackupStore () {
        this.backupData = null;
        this.backupStorer = null;
    }

    public SQLiteBackupStore (final BackupData backupData, final IBackupStorer backupStorer) {
        this.backupData = backupData;
        this.backupStorer = backupStorer;
    }

    @Override
    public String execute() {
        return this.backupData.store(this.backupStorer);
    }

    @Override
    public void undo() {
        this.backupData.undoStore();
    }
}
