package com.toyshopstudio.madmin.infra.data.backup;

/**
 * User: Saulo Júnior
 * Date: 9/22/15
 * Time: 6:28 PM
 */
public class SQLiteRecovery implements IRecovery {

    private BackupData backupData;

    public SQLiteRecovery (BackupData backupData) {
        this.backupData = backupData;
    }

    @Override
    public String execute() {
        return backupData.restore();
    }

    @Override
    public void undo() {
        this.backupData.undoRestore();
    }

}
