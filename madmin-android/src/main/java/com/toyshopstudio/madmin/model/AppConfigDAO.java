package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.infra.data.DataBase;
import com.toyshopstudio.madmin.infra.data.ContentValuesHolder;

/**
 *
 * Created by Saulo Junior on 7/15/2016.
 */
public class AppConfigDAO {

    private DataBase db;

    private AppConfigDAO(DataBase db){
        this.db = db;
    }

    public static AppConfigDAO newInstance(DataBase db){
        return new AppConfigDAO(db);
    }

    AppConfig find(){
        AppConfig appConfig = null;
        db.open();
        String queryStringBuilder = "SELECT ac._id, ac.access_counter " +
                " FROM " +
                AppConfig.getTableName() + " ac";
        Cursor cursor = db.executeQuery(queryStringBuilder, new String[]{});
        while (cursor.moveToNext()){
            appConfig = new AppConfig();
            appConfig.setId(cursor.getLong(0));
            appConfig.setAccessCounter(cursor.getLong(1));
        }
        cursor.close();
        db.close();
        return appConfig;
    }

    public AppConfig save(AppConfig appConfig){
        ContentValuesHolder fields = new ContentValuesHolder();
        db.open();
        fields.put("access_counter", appConfig.getAccessCounter());
        fields.put("_id", appConfig.getId());
        if(appConfig.getId() == null){
            db.save(AppConfig.getTableName(), fields);
        } else {
            db.update(AppConfig.getTableName(), fields);
        }
        appConfig = find();
        return appConfig;
    }
}
