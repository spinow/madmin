package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.AppConfig;

/**
 * Repository for the application settings
 * Created by Saulo Junior on 7/15/2016.
 */
public class AppConfigRepository {

    private static AppConfigRepository appConfigRepository;
    private static AppConfigDAO dao;

    private AppConfigRepository(AppConfigDAO appConfigDAO){
        dao = appConfigDAO;
    }

    public static AppConfigRepository newInstance(AppConfigDAO appConfigDAO){
        return new AppConfigRepository(appConfigDAO);
    }

    public AppConfig find(){
        return dao.find();
    }

    public AppConfig save(AppConfig appConfig){
        return dao.save(appConfig);
    }

}
