package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Domain;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.ContentValuesHolder;
import com.toyshopstudio.madmin.infra.data.DataBase;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 23:09
 */
public class BoxDAO implements MadminDAO<Box> {

    private DataBase db;
    private EntryDAO entryDAO;

    private BoxDAO(DataBase dataBase, EntryDAO entryDAO){
        this.db = dataBase;
        this. entryDAO = entryDAO;
    }

    public static BoxDAO newInstance(DataBase dataBase, EntryDAO entryDAO){
        return new BoxDAO(dataBase, entryDAO);
    }

    public Box save(Box box){
        ContentValuesHolder fields = new ContentValuesHolder();
        db.open();
        fields.put("description", box.getDescription());
        fields.put("_id", box.getId());
        if(box.getId() == null){
            db.save(Box.getTableName(), fields);
        } else {
            db.update(Box.getTableName(), fields);
        }
        box = findByDescription(box.getDescription());
        return box;
    }

    @Override
    public Collection<Box> findAll() {
        return findAll(null);
    }

    @Override
    public Box findById(final Long id, final DatePeriod datePeriod){
        Box box = new Box();
        final DatePeriod period = DatePeriod.getSelectedPeriod(datePeriod);
        final DatePeriod averagePeriod = DatePeriod.getSelectedPeriod(null);
        db.open();
        final String queryStringBuilder = "SELECT l._id, l.description, sum(e.value)" +

                // Subquery to gather averages:
                ", (SELECT AVG(values_sum) FROM (" +
                " SELECT strftime('%Y%m',e1.date_created/1000,'unixepoch'), SUM(e1.value) AS values_sum" +
                " FROM " +
                Entry.getTableName() + " e1" +
                " WHERE e1.label_id = l._id AND e1.type = 1" +
                " AND e1.date_created < ?" +
                " GROUP BY strftime('%Y%m',e1.date_created/1000,'unixepoch'))  AS average) " +

                // Subquery to gather income sum:
                ", (SELECT sum(e2.value) FROM " +
                Box.getTableName() + " AS l1" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e2 ON (e2.date_created BETWEEN ? AND ? AND e2.label_id = l1._id AND e2.type = 2) OR e2._id IS NULL" +
                " GROUP BY l1._id, l1.description" +
                " HAVING l1._id = l._id) AS income " +
                " FROM " +
                Box.getTableName() + " AS l" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e ON (e.date_created BETWEEN ? AND ? AND e.label_id = l._id AND e.type = 1) OR e._id IS NULL" +
                " GROUP BY l._id, l.description" +
                " HAVING l._id = ?";

        final Cursor cursor = db.executeQuery(queryStringBuilder, new String[]{String.valueOf(averagePeriod.getInitialDate().getTime()), String.valueOf(period.getInitialDate().getTime()), String.valueOf(period.getFinalDate().getTime()),String.valueOf(period.getInitialDate().getTime()), String.valueOf(period.getFinalDate().getTime()),id.toString()});
        while (cursor.moveToNext()){
            box = buildBox(cursor);
        }
        cursor.close();
        db.close();
        return box;
    }

    Box findByDescription(String description){
        Box box = null;
        final DatePeriod month = DatePeriod.getSelectedPeriod(null);
        db.open();
        final String queryStringBuilder = "SELECT l._id, l.description, sum(e.value)" +

                // Subquery to gather averages:
                ", (SELECT AVG(values_sum) FROM (" +
                " SELECT strftime('%Y%m',e1.date_created/1000,'unixepoch'), SUM(e1.value) AS values_sum" +
                " FROM " +
                Entry.getTableName() + " e1" +
                " WHERE e1.label_id = l._id AND e1.type = 1" +
                " AND e1.date_created < ?" +
                " GROUP BY strftime('%Y%m',e1.date_created/1000,'unixepoch'))  AS average) " +

                // Subquery to gather income sum:
                ", (SELECT sum(e2.value) FROM " +
                Box.getTableName() + " AS l1" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e2 ON (e2.date_created BETWEEN ? AND ? AND e2.label_id = l1._id AND e2.type = 2) OR e2._id IS NULL" +
                " GROUP BY l1._id, l1.description" +
                " HAVING l1._id = l._id) AS income " +
                " FROM " +
                Box.getTableName() + " AS l" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e ON (e.date_created BETWEEN ? AND ? AND e.label_id = l._id AND e.type = 1) OR e._id IS NULL" +
                " GROUP BY l._id, l.description" +
                " HAVING UPPER(l.description)=?";

        final Cursor cursor = db.executeQuery(
            queryStringBuilder, new String[]{
                String.valueOf(
                        month.getInitialDate().getTime()
                ), String.valueOf(month.getInitialDate().getTime()),
                        String.valueOf(month.getFinalDate().getTime()),
                        String.valueOf(month.getInitialDate().getTime()),
                        String.valueOf(month.getFinalDate().getTime()
                ), description.toUpperCase()
            }
        );

        while (cursor.moveToNext()){
            box = buildBox(cursor);
        }
        cursor.close();
        db.close();
        return box;
    }

    private Box buildBox(final Cursor cursor) {
        final Box box;
        box = new Box();
        box.setId(cursor.getLong(0));
        box.setDescription(cursor.getString(1));
        box.setOutgoSum(BigDecimal.valueOf(cursor.getDouble(2)));
        box.setOutgoMonthlyAverage(BigDecimal.valueOf(cursor.getDouble(3)));
        box.setIncomeSum(BigDecimal.valueOf(cursor.getDouble(4)));
        return box;
    }

    @Override
    public Collection<Box> findAll(final DatePeriod datePeriod){
        final Collection<Box> boxes = new ArrayList<Box>(0);
        final DatePeriod month = DatePeriod.getSelectedPeriod(datePeriod);
        final DatePeriod averagePeriod = DatePeriod.getSelectedPeriod(null);
        db.open();
        final String queryStringBuilder = "SELECT l._id, l.description, sum(e.value) " +

                // Subquery to gather averages:
                ", (SELECT AVG(values_sum) FROM (" +
                " SELECT strftime('%Y%m',e1.date_created/1000,'unixepoch'), SUM(e1.value) AS values_sum" +
                " FROM " +
                Entry.getTableName() + " e1" +
                " WHERE e1.label_id = l._id AND e1.type = 1" +
                " AND e1.date_created < ?" +
                " GROUP BY strftime('%Y%m',e1.date_created/1000,'unixepoch'))  AS average) " +

                // Subquery to gather income sum:
                ", (SELECT sum(e2.value) FROM " +
                Box.getTableName() + " AS l1" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e2 ON (e2.date_created BETWEEN ? AND ? AND e2.label_id = l1._id AND e2.type = 2) OR e2._id IS NULL" +
                " GROUP BY l1._id, l1.description" +
                " HAVING l1._id = l._id) AS income " +
                " FROM " +
                Box.getTableName() + " AS l" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e ON (e.date_created BETWEEN ? AND ? AND e.label_id = l._id AND e.type = 1) OR e._id IS NULL" +
                " GROUP BY l._id, l.description" +
                " ORDER BY UPPER(l.description) ASC";
        Cursor cursor = db.executeQuery(queryStringBuilder, new String[]{String.valueOf(averagePeriod.getInitialDate().getTime()), String.valueOf(month.getInitialDate().getTime()), String.valueOf(month.getFinalDate().getTime()),String.valueOf(month.getInitialDate().getTime()), String.valueOf(month.getFinalDate().getTime())});
        while (cursor.moveToNext()){
            Box box = new Box();
            box.setId(cursor.getLong(0));
            box.setDescription(cursor.getString(1));
            box.setOutgoSum(BigDecimal.valueOf(cursor.getDouble(2)));
            box.setOutgoMonthlyAverage(BigDecimal.valueOf(cursor.getDouble(3)));
            box.setIncomeSum(BigDecimal.valueOf(cursor.getDouble(4)));
            boxes.add(box);
        }
        cursor.close();
        db.close();
        return boxes;
    }

    @Override
    public boolean delete(final Box box){
        db.open();
        final Collection<Entry> entries = entryDAO.findAllByBox(box);
        final boolean deleted = db.delete(Box.getTableName(), box.getId());
        if(deleted){
            for(final Entry entry : entries){
                entryDAO.delete(entry);
            }
        }
        db.close();
        return deleted;
    }

    @Override
    public BigDecimal sum(final EntryType entryType, final DatePeriod datePeriod){
        BigDecimal sum = new BigDecimal(0.0);
        final DatePeriod month = DatePeriod.getSelectedPeriod(datePeriod);
        db.open();
        final String queryStringBuilder = "SELECT sum(e.value) FROM " +
                Box.getTableName() + " AS l" +
                " LEFT JOIN " +
                Entry.getTableName() + " AS e ON e.label_id IS l._id" +
                " WHERE" +
                " (e.date_created BETWEEN ? AND ?" +
                " OR e._id IS NULL)" +
                " AND e.type = " + entryType.getNumber();
        Cursor cursor = db.executeQuery(
                queryStringBuilder,
                new String[]{
                    String.valueOf(month.getInitialDate().getTime()),
                        String.valueOf(month.getFinalDate().getTime())
                }
                );
        while (cursor.moveToNext()){
            sum = BigDecimal.valueOf(cursor.getDouble(0));
        }
        cursor.close();
        db.close();
        return sum;
    }

    @Override
    public Collection<Box> findAllIncome(final DatePeriod datePeriod) {
        throw new NotImplementedException();
    }

    @Override
    public Collection<Box> findAllBy(final Domain domain, final DatePeriod datePeriod) {
        throw new NotImplementedException();
    }
}
