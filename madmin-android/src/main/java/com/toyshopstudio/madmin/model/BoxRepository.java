package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 16/08/14
 * Time: 22:26
 */
public class BoxRepository implements Repository<Box> {

    private static BoxRepository boxRepository;
    private static BoxDAO dao;
    private static EntryDAO entryDAO;

    BoxRepository(){
        throw new RuntimeException("Cannot invoke default constructor!");
    }

    private BoxRepository(BoxDAO boxDao){
        dao = boxDao;
    }

    public static BoxRepository newInstance(BoxDAO dao){
        return new BoxRepository(dao);
    }

    @Override
    public Box save(Box box){
        return dao.save(box);
    }

    public Box findById(Long id, DatePeriod datePeriod){
        return dao.findById(id, datePeriod);
    }

    public Box findByDescription(String description){
        return dao.findByDescription(description);
    }

    @Override
    public Collection<Box> findAll(DatePeriod datePeriod){
        return dao.findAll(datePeriod);
    }

    @Override
    public boolean delete(Box box){
        return dao.delete(box);
    }

    public BigDecimal sumOutgo(DatePeriod datePeriod){
        return dao.sum(EntryType.OUTGO, datePeriod);
    }

    public BigDecimal sumIncome(DatePeriod datePeriod){
        return dao.sum(EntryType.INCOME, datePeriod);
    }

}
