package com.toyshopstudio.madmin.model;

import android.database.Cursor;
import com.toyshopstudio.madmin.business.*;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.ContentValuesHolder;
import com.toyshopstudio.madmin.infra.data.DataBase;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * User: Saulo Junior
 * Date: 17/08/14
 * Time: 01:12
 */
public class EntryDAO implements MadminDAO<Entry> {

    private DataBase db;

    private EntryDAO(final DataBase dataBase){
        this.db = dataBase;
    }

    public static EntryDAO newInstance(final DataBase dataBase){
        return new EntryDAO(dataBase);
    }

    @Override
    public Entry findById(final Long id, final DatePeriod datePeriod) {
        throw new NotImplementedException();
    }

    @Override
    public Entry save(final Entry entry){
        final ContentValuesHolder fields = new ContentValuesHolder();
        db.open();
        fields.put("description", entry.getDescription());
        fields.put("value", Double.parseDouble(entry.getValue().toString()));
        fields.put("_id", entry.getId());
        fields.put("type", entry.getType().getNumber());
        if(entry.getBox() != null) {
            fields.put("label_id", entry.getBox().getId());
        }
        fields.put("date_created", entry.getDateCreated() == null ? new Date().getTime() : entry.getDateCreated().getTime());
        if(entry.getId() == null){
            db.save(Entry.getTableName(), fields);
        } else {
            db.update(Entry.getTableName(), fields);
        }
        db.close();
        return entry;
    }

    @Override
    public Collection<Entry> findAll(){
        final Collection<Entry> entries = new ArrayList<Entry>(0);
        db.open();
        final Cursor cursor = db.findAll(Entry.getTableName(), new String[]{"type", "_id", "description", "value", "label_id", "date_created"}, "UPPER(description) ASC");
        while (cursor.moveToNext()){
            buildEntry(entries,cursor);
        }
        cursor.close();
        db.close();
        return entries;
    }

    @Override
    public Collection<Entry> findAll(final DatePeriod datePeriod) {
        return findAll();
    }

    Collection<Entry> findAllByBox(final Box box){
        final Collection<Entry> entries = new ArrayList<Entry>(0);
        db.open();
        final StringBuilder queryStringBuilder = new StringBuilder("SELECT type, _id, description, value, label_id, date_created FROM ")
                .append(Entry.getTableName()).append(" WHERE label_id = ?");
        queryStringBuilder.append(" ORDER BY _id DESC");
        final Cursor cursor = db.executeQuery(queryStringBuilder.toString(), new String[]{box.getId().toString()});
        while (cursor.moveToNext()){
            buildEntry(entries, cursor);
        }
        cursor.close();
        db.close();
        return entries;
    }

    /**
     * This method does not finds the @{link {@link Box}} from database.
     * It only fills its ID. Therefore, it may occur equals and hashcode incompatibility between two compared Entries.
     * @param entries reference of the collection to be filled with entries.
     * @param cursor used in the iteration to fill a collection of entries.
     */
    private void buildEntry(final Collection<Entry> entries, final Cursor cursor) {
        final Box box = new Box();
        final Integer type = cursor.getInt(0);
        final Entry entry = createEntry(type);
        entry.setId(cursor.getLong(1));
        entry.setDescription(cursor.getString(2));
        entry.setValue(BigDecimal.valueOf(cursor.getDouble(3)));
        box.setId(cursor.getLong(4));
        entry.setDateCreated(new Date(cursor.getLong(5)));
        entry.setBox(box);
        entries.add(entry);
    }

    @Override
    public Collection<Entry> findAllBy(final Domain box, final DatePeriod datePeriod){
        final Collection<Entry> entries = new ArrayList<Entry>(0);
        db.open();
        final DatePeriod period = DatePeriod.getSelectedPeriod(datePeriod);
        final StringBuilder queryStringBuilder = new StringBuilder("SELECT type, _id, description, value, label_id, date_created FROM ")
                .append(Entry.getTableName()).append(" WHERE label_id = ?");
        queryStringBuilder.append(" and date_created BETWEEN ? and ?").append(" ORDER BY _id DESC");
        final Cursor cursor = db.executeQuery(queryStringBuilder.toString(), new String[]{box.getId().toString(), String.valueOf(period.getInitialDate().getTime()), String.valueOf(period.getFinalDate().getTime())});
        while (cursor.moveToNext()){
            buildEntry(entries, cursor);
        }
        cursor.close();
        db.close();
        return entries;
    }

    @Override
    public Collection<Entry> findAllIncome(final DatePeriod datePeriod){
        final Collection<Entry> entries = new ArrayList<Entry>(0);
        db.open();
        final DatePeriod period = DatePeriod.getSelectedPeriod(datePeriod);
        final String queryStringBuilder = "SELECT type, _id, description, value, label_id, date_created FROM " +
                Entry.getTableName() +
                " WHERE date_created BETWEEN ? and ?" +
                " and type = 2" +
                " ORDER BY _id DESC";
        final Cursor cursor = db.executeQuery(queryStringBuilder, new String[]{String.valueOf(period.getInitialDate().getTime()), String.valueOf(period.getFinalDate().getTime())});
        while (cursor.moveToNext()){
            buildEntry(entries, cursor);
        }
        cursor.close();
        db.close();
        return entries;
    }

    private Entry createEntry(final Integer type) {
        final Entry entry;
        if(EntryType.OUTGO.getNumber().equals(type)) {
            entry = new Outgo();
        } else if (EntryType.INCOME.getNumber().equals(type)) {
            entry = new Income();
        } else {
            throw new EntryTypeException();
        }
        return entry;
    }

    public boolean delete(final Entry entry){
        db.open();
        final boolean deleted = db.delete(Entry.getTableName(), entry.getId());
        db.close();
        return deleted;
    }

    @Override
    public BigDecimal sum(final EntryType entryType, final DatePeriod datePeriod) {
        BigDecimal sum = new BigDecimal(0);
        db.open();
        final DatePeriod period = DatePeriod.getSelectedPeriod(datePeriod);
        final StringBuilder queryStringBuilder = new StringBuilder("SELECT SUM(value) FROM ").append(Entry.getTableName()).append(" WHERE type = ").append(entryType.getNumber());
        queryStringBuilder.append(" and date_created BETWEEN ? and ?");
        final Cursor cursor = db.executeQuery(queryStringBuilder.toString(), new String[]{String.valueOf(period.getInitialDate().getTime()), String.valueOf(period.getFinalDate().getTime())});
        while (cursor.moveToNext()){
            sum = BigDecimal.valueOf(cursor.getDouble(0));
        }
        cursor.close();
        db.close();
        return sum;
    }

}
