package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 21:11
 */
public class EntryRepository implements Repository<Entry> {

    private static MadminDAO<Entry> dao;

    EntryRepository(){
        throw new RuntimeException("Cannot invoke default constructor!");
    }

    private EntryRepository(MadminDAO<Entry> entryDAO){
        dao = entryDAO;
    }

    public static EntryRepository newInstance(MadminDAO<Entry> entryDAO){
        return new EntryRepository(entryDAO);
    }

    @Override
    public Entry save(Entry entry){
        return dao.save(entry);
    }

    public Collection<Entry> findAll(DatePeriod datePeriod){
        return dao.findAll();
    }

    public Collection<Entry> findAllByBox(Box box, DatePeriod datePeriod){
        return dao.findAllBy(box, datePeriod);
    }

    public Collection<Entry> findAllIncomes(DatePeriod datePeriod){
        return dao.findAllIncome(datePeriod);
    }

    @Override
    public boolean delete(Entry entry){
        return dao.delete(entry);
    }

    public BigDecimal sumIncome(DatePeriod datePeriod) {
        return dao.sum(EntryType.INCOME, datePeriod);
    }

}
