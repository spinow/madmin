package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.Domain;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;

import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * Created by Saulo Junior on 8/12/2016.
 */
interface MadminDAO<T extends Domain>  {

    public T findById(final Long id, final DatePeriod datePeriod);

    public T save(final T entry);

    public Collection<T> findAll();

    public Collection<T> findAll(final DatePeriod datePeriod);

    public boolean delete(final T entry);

    public Collection<T> findAllIncome(final DatePeriod datePeriod);

    public Collection<T> findAllBy(final Domain domain, final DatePeriod datePeriod);

    public BigDecimal sum(final EntryType entryType, final DatePeriod datePeriod);

}
