package com.toyshopstudio.madmin.model;

import com.toyshopstudio.madmin.business.Domain;
import com.toyshopstudio.madmin.business.EntryType;
import com.toyshopstudio.madmin.infra.DatePeriod;

import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * Created by Saulo Junior on 8/4/2016.
 */
public interface Repository<T extends Domain> {

    public T save(T domain);

    public Collection<T> findAll(DatePeriod datePeriod);

    public boolean delete(T domain);

}
