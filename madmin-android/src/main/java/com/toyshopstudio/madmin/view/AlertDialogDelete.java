package com.toyshopstudio.madmin.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.toyshopstudio.madmin.R;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 03/09/14
 * Time: 00:04
 * To change this template use File | Settings | File Templates.
 */
public class AlertDialogDelete {

    private Context context;
    private String message = "";
    DialogInterface.OnClickListener positiveButtonListener;
    DialogInterface.OnClickListener neutralButtonListener;

    public AlertDialogDelete(Context context, String message,
                             DialogInterface.OnClickListener positiveButtonListener,
                             DialogInterface.OnClickListener neutralButtonListener){
        this.context = context;
        this.message = message;
        this.positiveButtonListener = positiveButtonListener;
        this.neutralButtonListener = neutralButtonListener;
    }

    public void show(){
        String title = context.getString(R.string.txt_delete);
        String ok = context.getString(R.string.txt_yes);
        String no = context.getString(R.string.txt_no);
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(ok,this.positiveButtonListener)
                .setNeutralButton(no,this.neutralButtonListener)
                .create()
                .show();
    }
}
