package com.toyshopstudio.madmin.view;

import android.content.Context;
import android.content.DialogInterface;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.service.BoxService;
import com.toyshopstudio.madmin.infra.data.DataBase;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.BoxDAO;
import com.toyshopstudio.madmin.model.BoxRepository;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;

/**
 * User: Saulo Junior
 * Date: 03/09/14
 * Time: 00:20
 */
public class OnClickListenerDeleteBoxConfirm implements DialogInterface.OnClickListener {

    private Box box;
    private BoxService boxService;
    private Context context;

    public OnClickListenerDeleteBoxConfirm(Context context, Box box){
        DataBase dataBase = SQLiteDataBaseImpl.getInstance();
        EntryDAO entryDAO = EntryDAO.newInstance(dataBase);
        this.boxService = BoxService.newInstance(
            BoxRepository.newInstance(
                BoxDAO.newInstance(dataBase, entryDAO)
            )
        );
        this.box = box;
        this.context = context;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        String message = context.getString(R.string.txt_deleted_box);
        this.boxService.delete(box);
        ((ActivityBoxList)context).refreshListData(null);
        ((ActivityBoxList)context).setSelectedBoxIndex(0);
        ToastOnMiddle.makeText(context,message,Integer.parseInt(context.getString(R.string.app_toast_time))).show();
    }
}
