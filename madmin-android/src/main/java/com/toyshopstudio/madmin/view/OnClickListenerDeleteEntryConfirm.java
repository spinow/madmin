package com.toyshopstudio.madmin.view;

import android.content.Context;
import android.content.DialogInterface;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;

/**
 * User: Saulo Junior
 * Date: 03/09/14
 * Time: 00:20
 */
public class OnClickListenerDeleteEntryConfirm implements DialogInterface.OnClickListener {

    private Entry entry;
    private EntryService entryService;
    private Context context;

    public OnClickListenerDeleteEntryConfirm(Context context, Entry entry){
        this.entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        this.entry = entry;
        this.context = context;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        String message = context.getString(R.string.txt_deleted_entry);
        this.entryService.delete(entry);
        ((ToyshopActivity)context).refreshListData(entry.getBox().getId());
        ToastOnMiddle.makeText(context,message,Integer.parseInt(context.getString(R.string.app_toast_time))).show();
    }
}
