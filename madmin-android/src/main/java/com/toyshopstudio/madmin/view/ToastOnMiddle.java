package com.toyshopstudio.madmin.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 04/09/14
 * Time: 20:52
 * To change this template use File | Settings | File Templates.
 */
public class ToastOnMiddle {

    public ToastOnMiddle(Context context) {
    }

    public static Toast makeText(Context context, CharSequence text, int duration){
        Toast toast = Toast.makeText(context,text,duration);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 10);
        return toast;
    }

}
