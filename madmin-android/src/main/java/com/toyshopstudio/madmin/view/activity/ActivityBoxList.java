package com.toyshopstudio.madmin.view.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.service.BoxService;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.ToyshopFormatTool;
import com.toyshopstudio.madmin.infra.data.DataBase;
import com.toyshopstudio.madmin.infra.data.DataBaseHelper;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.infra.data.backup.*;
import com.toyshopstudio.madmin.model.BoxDAO;
import com.toyshopstudio.madmin.model.BoxRepository;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.AlertDialogDelete;
import com.toyshopstudio.madmin.view.OnClickListenerDeleteBoxConfirm;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;
import com.toyshopstudio.madmin.view.adapter.BoxAdapter;
import com.toyshopstudio.madmin.view.dialog.*;

import java.io.*;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ActivityBoxList extends ToyshopActivity {

    public static final String SELECTED_BOX_ID = "selected_box_id";
    public static final String SELECTED_DATE_PERIOD = "selected_date_period";
    public static final int FILE_SELECT_CODE = 0;

    private BoxService boxService;
    private LinearLayout layoutBoxSubHeader;
    private LinearLayout layoutBoxLabelsSubHeader;
    private EntryService entryService;
    private ImageButton btnMainMenu, btnAddMonthIncome;
    private TextView txtSelectedDatePeriodMonth, txtSelectedDatePeriodYear, txtMonthIncomeSum, txtMonthOutgoSum, txtMonthBalance;
    private static BoxRepository boxRepository;
    private int selectedBoxIndex;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.box_list_view);

        context = this;
        DataBase dataBase = SQLiteDataBaseImpl.getInstance();
        EntryDAO entryDAO = EntryDAO.newInstance(dataBase);

        boxService = BoxService.newInstance(
                BoxRepository.newInstance(
                        BoxDAO.newInstance(dataBase, entryDAO)
                )
        );
        entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        initLayout();

        receiveSelectedDatePeriodBroadcast();

        registerForContextMenu(getListView());

        final BoxAdapter adapter = new BoxAdapter(this, getData());
        setListAdapter(adapter);

        registerForContextMenu(this.btnMainMenu);

        increaseAccessCounter();
    }

    protected void onRestoreInstanceState(android.os.Bundle state) {
        selectedDatePeriod = (DatePeriod)state.get(ActivityBoxList.SELECTED_DATE_PERIOD);
    }

    @Override
    protected void onNewIntent(android.content.Intent intent) {
        selectedDatePeriod = (DatePeriod)intent.getSerializableExtra(ActivityBoxList.SELECTED_DATE_PERIOD);
        refreshListData(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void refreshListData(Long domainId) {
        Collection<Box> boxes = getData();
        setLayoutVisibility();
        BoxAdapter adapter = new BoxAdapter(context, boxes);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void initLayout() {
        this.txtSelectedDatePeriodMonth = (TextView)findViewById(R.id.txt_selected_date_period_month);
        this.txtSelectedDatePeriodYear = (TextView)findViewById(R.id.txt_selected_date_period_year);
        this.txtMonthIncomeSum = (TextView)findViewById(R.id.txt_month_income_sum);
        this.txtMonthOutgoSum = ((TextView)findViewById(R.id.txt_month_outgo_sum));
        this.txtMonthBalance = ((TextView)findViewById(R.id.txt_month_balance));
        this.btnMainMenu = ((ImageButton)findViewById(R.id.btn_main_menu));
        this.btnAddMonthIncome = ((ImageButton)findViewById(R.id.btn_add_month_income));
        this.layoutBoxSubHeader = (LinearLayout)findViewById(R.id.layout_box_sub_header);
        this.layoutBoxLabelsSubHeader = (LinearLayout)findViewById(R.id.layout_box_labels_subheader);
    }

    private void setLayoutVisibility() {
        if ("".equals(this.txtMonthOutgoSum.getText()) && "".equals(this.txtMonthBalance.getText())) {
            this.layoutBoxSubHeader.setVisibility(View.INVISIBLE);
            this.layoutBoxLabelsSubHeader.setVisibility(View.INVISIBLE);
            this.layoutBoxSubHeader.getLayoutParams().height = 0;
            this.layoutBoxLabelsSubHeader.getLayoutParams().height = 0;
        } else {
            this.layoutBoxSubHeader.setVisibility(View.VISIBLE);
            this.layoutBoxLabelsSubHeader.setVisibility(View.VISIBLE);
            this.layoutBoxSubHeader.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            this.layoutBoxLabelsSubHeader.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }
        this.btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu(view);
            }
        });

        this.btnAddMonthIncome.setOnClickListener(new View.OnClickListener() {

            Dialog dialogAddIncome = new DialogAddMonthIncome(context);

            @Override
            public void onClick(View view) {
                if (DatePeriod.isSelectedPeriodCurrent(selectedDatePeriod)) {
                    dialogAddIncome.show();
                }
                else {
                    ToastOnMiddle.makeText(context, context.getString(R.string.txt_operation_not_permitted_not_actual_period), Integer.parseInt(context.getString(R.string.app_toast_time_long))).show();
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        context = this;
    }

    /**
     * @return the data to be inflated in the list view.
     */
    private Collection<Box> getData() {

        Collection<Box> boxes = boxService.getAll(getSelectedDatePeriod());
        if(boxes.isEmpty() && getAppConfig().getAccessCounter() == 0){
            //Insert default data if first access.
            boxes = DataBaseHelper.insertDefaultBoxData(this);
        }
        if(getListView().getHeaderViewsCount() == 0) {
            getListView().addHeaderView(new View(this));
        }
        BigDecimal outgoSum = boxService.getOutgoSum(boxes);
        BigDecimal incomeSum = entryService.getSelectedPeriodIncomeSum(getSelectedDatePeriod());

        txtMonthOutgoSum.setText(boxService.getFormattedOutgoSum(outgoSum, getSelectedDatePeriod()));
        txtMonthIncomeSum.setText(entryService.getFormattedSelectedPeriodIncomeSum(getSelectedDatePeriod()));
        txtMonthBalance.setText(entryService.getFormattedSelectedPeriodBalance(incomeSum, outgoSum));
        txtSelectedDatePeriodMonth.setText(ToyshopFormatTool.formattedDateMonth(context, getSelectedDatePeriod().getInitialDate()));
        txtSelectedDatePeriodYear.setText(ToyshopFormatTool.formattedDateYear(getSelectedDatePeriod().getInitialDate()));

        this.txtSelectedDatePeriodMonth.setOnClickListener(this.selectedDateOnClickListener);
        this.txtSelectedDatePeriodYear.setOnClickListener(this.selectedDateOnClickListener);
        this.txtMonthIncomeSum.setOnClickListener(this.openIncomeListOnClickListener);

        return boxes;
    }

    private View.OnClickListener openIncomeListOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openIncomeListActivity();
        }
    };

    @Override
    public void onCreateContextMenu(android.view.ContextMenu menu, android.view.View v, android.view.ContextMenu.ContextMenuInfo menuInfo) {
        menu.close();
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        if(v.getId() == getListView().getId()) {
            inflater.inflate(R.menu.context_menu_box, menu);
        } else if (v.getId() == R.id.btn_main_menu) {
            inflater.inflate(R.menu.main_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {
        Box box = ((Box)getListView().getItemAtPosition(selectedBoxIndex));
        if(!isFinishing()) {
            if (item.getItemId() == R.id.box_edit) {
                Dialog dialogEditBox = new DialogEditBox(context, box);
                dialogEditBox.show();
            } else if (item.getItemId() == R.id.box_delete) {
                new AlertDialogDelete(context, context.getString(R.string.txt_delete_box_confirm), new OnClickListenerDeleteBoxConfirm(context, box), null).show();
            } else if (item.getItemId() == R.id.main_menu_add_box) {
                Dialog dialogAddBox = new DialogAddBox(context);
                dialogAddBox.show();
            } else if (item.getItemId() == R.id.main_menu_delete_month_income) {
                openIncomeListActivity();
            } else if (item.getItemId() == R.id.main_menu_select_period) {
                Dialog dialogSelectPeriod = new DialogSelectPeriod(context, getSelectedDatePeriod());
                dialogSelectPeriod.show();
            } else if (item.getItemId() == R.id.main_menu_backup_data) {
                Dialog dialogDataBackup = new DialogDataBackup(context);
                dialogDataBackup.show();
            } else if (item.getItemId() == R.id.main_menu_help) {
                openHelpActivity();
            }
        }
        return true;
    }

    private void openHelpActivity() {
        Intent intent = new Intent(this, ActivityHelp.class);
        ActivityBoxList.this.startActivity(intent);
    }

    private void openIncomeListActivity() {
        Intent intent = new Intent(this, ActivityIncomeList.class);
        intent.putExtra(ActivityBoxList.SELECTED_DATE_PERIOD, getSelectedDatePeriod());
        ActivityBoxList.this.startActivity(intent);
    }

    @Override
    public void onResume () {
        try {
            unregisterReceiver(selectedDatePeriodBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            //TODO: Implementar mecanismo que checa se o BroadcastReceiver foi registrado.
        }
        receiveSelectedDatePeriodBroadcast();
        refreshListData(null);
        super.onResume();
    }

    public int getSelectedBoxIndex() {
        return selectedBoxIndex;
    }

    public void setSelectedBoxIndex(int selectedBoxIndex) {
        this.selectedBoxIndex = selectedBoxIndex;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    try {
                        InputStream fileInputStream = getContentResolver().openInputStream(data.getData());
                        File tempFile = File.createTempFile(uri.getLastPathSegment(), null, context.getCacheDir());

                        FileChannel originChannel = ((FileInputStream)fileInputStream).getChannel();
                        FileOutputStream destOutput = new FileOutputStream(tempFile);
                        FileChannel destChannel = destOutput.getChannel();
                        originChannel.transferTo(0, fileInputStream.available(), destChannel);

                        BackupFile file  = new BackupFile(new FileInputStream(tempFile), tempFile.getPath());
                        BackupData backupData = new SQLiteBackupData(context, file, new SQLiteBackupDataValidator(file));
                        DataSecurityEvent dataSecurityEvent = new SQLiteRecovery(backupData);
                        List<DataSecurityEvent> events = new ArrayList<DataSecurityEvent>(0);
                        events.add(dataSecurityEvent);
                        new BackupDataAsyncTask(context, events).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                        ToastOnMiddle.makeText(context, context.getString(R.string.recover_fail), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } catch (BackupDataException bde) {
                        ToastOnMiddle.makeText(context, getExceptionMessageValue(bde.getMessage()), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
