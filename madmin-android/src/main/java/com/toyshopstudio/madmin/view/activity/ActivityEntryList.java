package com.toyshopstudio.madmin.view.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.*;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.service.BoxService;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.DataBase;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.BoxDAO;
import com.toyshopstudio.madmin.model.BoxRepository;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.AlertDialogDelete;
import com.toyshopstudio.madmin.view.OnClickListenerDeleteEntryConfirm;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;
import com.toyshopstudio.madmin.view.adapter.EntryAdapter;
import com.toyshopstudio.madmin.view.dialog.DialogAddIncome;
import com.toyshopstudio.madmin.view.dialog.DialogAddOutgo;

import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 04/09/14
 * Time: 21:28
 */
public class ActivityEntryList extends ToyshopActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private ImageButton btnAddOutgo, btnAddIncome, btnBack;
    private LinearLayout layoutEntrySubHeader;
    private LinearLayout layoutEntryLabelsSubHeader;
    private int selectedEntryIndex;
    private Box selectedBox;
    private static EntryService entryService;
    private static BoxService boxService;
    private DatePeriod selectedDatePeriod;
    private TextView txtEntryListViewTitle,txtEntryListViewOutgoSum, txtEntryListViewIncomeSum, txtEntryListViewBalance, txtEntryListViewAverage;
    private ListView listEntry;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_list_view);

        context = this;
        entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        final DataBase dataBase = SQLiteDataBaseImpl.getInstance();
        final EntryDAO entryDAO = EntryDAO.newInstance(dataBase);
        boxService = BoxService.newInstance(
                BoxRepository.newInstance(
                        BoxDAO.newInstance(dataBase, entryDAO)
                )
        );
        initLayout();

        selectedDatePeriod = (DatePeriod)getIntent().getSerializableExtra(ActivityBoxList.SELECTED_DATE_PERIOD);
        refreshListData(getSelectedBoxId());

        this.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private Long getSelectedBoxId(){
        return getIntent().getLongExtra(ActivityBoxList.SELECTED_BOX_ID, 0L);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void refreshListData(Long boxId) {
        getListView().setOnItemLongClickListener(this);
        registerForContextMenu(getListView());
        if(getListView().getHeaderViewsCount() == 0) {
            getListView().addHeaderView(new View(this));
        }

        Collection<Entry> entries = getData(boxId, selectedDatePeriod);

        setLayoutVisibility();

        final EntryAdapter adapter = new EntryAdapter(this, entries);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void initLayout() {
        this.layoutEntrySubHeader = (LinearLayout)findViewById(R.id.layout_entry_sub_header);
        this.layoutEntryLabelsSubHeader = (LinearLayout)findViewById(R.id.layout_entry_labels_subheader);
        this.txtEntryListViewTitle = (TextView)findViewById(R.id.txt_entry_list_view_title);
        this.txtEntryListViewOutgoSum = (TextView)findViewById(R.id.txt_entry_list_view_outgo_sum);
        this.txtEntryListViewIncomeSum = (TextView)findViewById(R.id.txt_entry_list_view_income_sum);
        this.txtEntryListViewBalance = (TextView)findViewById(R.id.txt_entry_list_view_balance);
        this.txtEntryListViewAverage = (TextView)findViewById(R.id.txt_entry_list_view_average);
        this.btnAddOutgo = (ImageButton)findViewById(R.id.btn_add_outgo);
        this.btnAddIncome = (ImageButton)findViewById(R.id.btn_add_income);
        this.btnBack = (ImageButton)findViewById(R.id.btn_back);
        this.listEntry = (ListView) findViewById(android.R.id.list);
    }

    private void setLayoutVisibility () {
        if ("".equals(this.txtEntryListViewIncomeSum.getText()) &&
                "".equals(this.txtEntryListViewOutgoSum.getText()) &&
                "".equals(this.txtEntryListViewBalance.getText()) &&
                "".equals(this.txtEntryListViewAverage.getText())) {
            this.layoutEntrySubHeader.setVisibility(View.INVISIBLE);
            this.layoutEntryLabelsSubHeader.setVisibility(View.INVISIBLE);
            this.listEntry.setVisibility(View.INVISIBLE);
        } else {
            this.layoutEntrySubHeader.setVisibility(View.VISIBLE);
            this.layoutEntryLabelsSubHeader.setVisibility(View.VISIBLE);
            this.listEntry.setVisibility(View.VISIBLE);
        }
        this.btnAddOutgo.setOnClickListener(new View.OnClickListener() {
            Dialog dialogAddOutgo = new DialogAddOutgo(context, selectedBox);

            @Override
            public void onClick(View view) {
                setBtnAddOutgoAction(dialogAddOutgo);
            }
        });

        this.btnAddIncome.setOnClickListener(new View.OnClickListener() {
            Dialog dialogAddIncome = new DialogAddIncome(context, selectedBox);

            @Override
            public void onClick(View view) {
                 setBtnAddIncomeAction(dialogAddIncome);
            }
        });
    }

    private void setBtnAddOutgoAction (Dialog dialogAddOutgo) {
        if (DatePeriod.isSelectedPeriodCurrent(selectedDatePeriod)) {
            dialogAddOutgo.show();
        }
        else {
            ToastOnMiddle.makeText(context, context.getString(R.string.txt_operation_not_permitted_not_actual_period), Integer.parseInt(context.getString(R.string.app_toast_time_long))).show();
        }
    }

    private void setBtnAddIncomeAction (Dialog dialogAddIncome) {
        if (DatePeriod.isSelectedPeriodCurrent(selectedDatePeriod)) {
            dialogAddIncome.show();
        }
        else {
            ToastOnMiddle.makeText(context, context.getString(R.string.txt_operation_not_permitted_not_actual_period), Integer.parseInt(context.getString(R.string.app_toast_time_long))).show();
        }
    }

    /**
     * @return the data to be inflated in the list view.
     */
    private Collection<Entry> getData(Long boxId, DatePeriod datePeriod) {

        selectedBox = boxService.getById(boxId, selectedDatePeriod);

        txtEntryListViewTitle.setText(getString(R.string.txt_entry_list_view_title, selectedBox.getDescription(30).toUpperCase()));
        txtEntryListViewOutgoSum.setText(selectedBox.getFormattedOutgoSum());
        txtEntryListViewIncomeSum.setText(selectedBox.getFormattedIncomeSum());
        txtEntryListViewBalance.setText(selectedBox.getFormattedBalance());
        txtEntryListViewAverage.setText(selectedBox.getFormattedMonthlyAverage());

        return entryService.getAllByBox(selectedBox, datePeriod);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedEntryIndex = i;
        openContextMenu(getListView());
        return false;
    }

    @Override
    public void onCreateContextMenu(android.view.ContextMenu menu, android.view.View v, android.view.ContextMenu.ContextMenuInfo menuInfo) {
        menu.close();
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_entry, menu);
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {
        if (DatePeriod.isSelectedPeriodCurrent(selectedDatePeriod)) {
            Entry entry = ((Entry)getListView().getItemAtPosition(selectedEntryIndex));
            new AlertDialogDelete(context, context.getString(R.string.txt_delete_expense_confirm), new OnClickListenerDeleteEntryConfirm(context,entry), null).show();
        }
        else {
            ToastOnMiddle.makeText(context, context.getString(R.string.txt_operation_not_permitted_not_actual_period), Integer.parseInt(context.getString(R.string.app_toast_time_long))).show();
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedEntryIndex = i;
        Dialog dialogAddEntry = new DialogAddOutgo(context, this.selectedBox);
        dialogAddEntry.show();
    }
}
