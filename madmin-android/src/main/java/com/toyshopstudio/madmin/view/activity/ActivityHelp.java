package com.toyshopstudio.madmin.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.view.activity.component.MadminWebViewClient;

/**
 * User: Saulo Junior
 * Date: 03/03/15
 * Time: 23:12
 */
public class ActivityHelp extends Activity {

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        WebView webView = (WebView) findViewById(R.id.help_webview);
        webView.setWebViewClient(new MadminWebViewClient());
        webView.loadUrl(this.getString(R.string.help_url));
    }

}
