package com.toyshopstudio.madmin.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.*;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.ToyshopFormatTool;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.AlertDialogDelete;
import com.toyshopstudio.madmin.view.OnClickListenerDeleteEntryConfirm;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;
import com.toyshopstudio.madmin.view.adapter.EntryAdapter;

import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 03/03/15
 * Time: 23:12
 */
public class ActivityIncomeList extends ToyshopActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private ImageButton btnBack;
    private int selectedEntryIndex;
    private static EntryService entryService;
    private TextView txtIncomeSum, txtSelectedDatePeriodMonth, txtSelectedDatePeriodYear;
    private ListView listIncome;
    private LinearLayout layoutEntrySubHeader;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.income_list_view);

        context = this;
        entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        initLayout();

        Intent intent = getIntent();
        selectedDatePeriod = (DatePeriod)intent.getSerializableExtra(ActivityBoxList.SELECTED_DATE_PERIOD);

        refreshListData(null);

        this.btnBack = ((ImageButton)findViewById(R.id.btn_back));
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    @Override
    public void refreshListData(Long domainId) {

        getListView().setOnItemLongClickListener(this);
        registerForContextMenu(getListView());
        if(getListView().getHeaderViewsCount() == 0) {
            getListView().addHeaderView(new View(this));
        }

        Collection<Entry> incomes = getData(getSelectedDatePeriod());
        setLayoutVisibility();

        final EntryAdapter adapter = new EntryAdapter(this, incomes);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void initLayout() {
        this.txtSelectedDatePeriodMonth = ((TextView)findViewById(R.id.txt_selected_date_period_month));
        this.txtSelectedDatePeriodYear = ((TextView)findViewById(R.id.txt_selected_date_period_year));
        this.txtIncomeSum = ((TextView)findViewById(R.id.txt_income_list_view_income_sum));
        this.listIncome = (ListView)findViewById(android.R.id.list);
        this.layoutEntrySubHeader = (LinearLayout)findViewById(R.id.layout_income_sub_header);
    }

    private void setLayoutVisibility () {
        if ("".equals(this.txtIncomeSum.getText())) {
            this.layoutEntrySubHeader.setVisibility(View.INVISIBLE);
            this.listIncome.setVisibility(View.INVISIBLE);
        } else {
            this.layoutEntrySubHeader.setVisibility(View.VISIBLE);
            this.listIncome.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Returns the data to be inflated in the list view.
     * @return
     */
    private Collection<Entry> getData(DatePeriod datePeriod) {
        Collection<Entry> incomes = entryService.getAllIncomes(datePeriod);

        this.txtIncomeSum.setText(entryService.getFormattedSelectedPeriodIncomeSum(entryService.sumIncomes(incomes), null));
        txtSelectedDatePeriodMonth.setText(ToyshopFormatTool.formattedDateMonth(context, getSelectedDatePeriod().getInitialDate()));
        txtSelectedDatePeriodYear.setText(ToyshopFormatTool.formattedDateYear(getSelectedDatePeriod().getInitialDate()));
        this.txtSelectedDatePeriodMonth.setOnClickListener(this.selectedDateOnClickListener);
        this.txtSelectedDatePeriodYear.setOnClickListener(this.selectedDateOnClickListener);

        return incomes;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedEntryIndex = i;
        openContextMenu(getListView());
        return false;
    }

    @Override
    public void onCreateContextMenu(android.view.ContextMenu menu, View v, android.view.ContextMenu.ContextMenuInfo menuInfo) {
        menu.close();
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_entry, menu);
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {
        if (DatePeriod.isSelectedPeriodCurrent(selectedDatePeriod)) {
            Entry income = ((Entry)getListView().getItemAtPosition(selectedEntryIndex));
            new AlertDialogDelete(context, context.getString(R.string.txt_delete_income_confirm), new OnClickListenerDeleteEntryConfirm(context,income), null).show();
        }
        else {
            ToastOnMiddle.makeText(context, context.getString(R.string.txt_operation_not_permitted_not_actual_period), Integer.parseInt(context.getString(R.string.app_toast_time_long))).show();
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) { }

    @Override
    public void onResume () {
        receiveSelectedDatePeriodBroadcast();
        refreshListData(null);
        super.onResume();
    }

}
