package com.toyshopstudio.madmin.view.activity.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import com.toyshopstudio.madmin.business.AppConfig;
import com.toyshopstudio.madmin.business.service.AppConfigService;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.AppConfigDAO;
import com.toyshopstudio.madmin.model.AppConfigRepository;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;
import com.toyshopstudio.madmin.view.dialog.DialogSelectPeriod;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 21:35
 */
public abstract class ToyshopActivity extends ToyshopListActivity {

    public static String SELECTED_DATE_PERIOD_INTENT = "android.intent.action.MAIN";

    protected DatePeriod selectedDatePeriod;
    protected BroadcastReceiver selectedDatePeriodBroadcastReceiver;
    private AppConfigService appConfigService;

    public abstract void refreshListData(Long domainId);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appConfigService = appConfigService == null ?
                AppConfigService.newInstance(
                        AppConfigRepository.newInstance(
                                AppConfigDAO.newInstance(SQLiteDataBaseImpl.getInstance())
                        )
                ) : appConfigService;
    }

    public DatePeriod getSelectedDatePeriod() {
        if(selectedDatePeriod == null) {
            selectedDatePeriod = DatePeriod.getSelectedPeriod(selectedDatePeriod);
        }
        return selectedDatePeriod;
    }

    protected View.OnClickListener selectedDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Dialog dialogSelectPeriod = new DialogSelectPeriod(context, getSelectedDatePeriod());
            dialogSelectPeriod.show();
        }
    };

    protected void receiveSelectedDatePeriodBroadcast() {
        IntentFilter selectedDatePeriodIntentFilter = new IntentFilter(SELECTED_DATE_PERIOD_INTENT);
        selectedDatePeriodBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                selectedDatePeriod = (DatePeriod)intent.getSerializableExtra(ActivityBoxList.SELECTED_DATE_PERIOD);
                refreshListData(null);
            }
        };
        registerReceiver(selectedDatePeriodBroadcastReceiver,selectedDatePeriodIntentFilter);
    }

    @Override
    protected void onStop()
    {
        if(selectedDatePeriodBroadcastReceiver != null) {
            unregisterReceiver(selectedDatePeriodBroadcastReceiver);
        }
        super.onStop();
    }

    /**
     * Intended to instantiate and initialize views.
     */
    protected abstract void initLayout ();

    /**
     * @return the application settings
     */
    protected AppConfig getAppConfig () {
        return appConfigService.getAppConfig();
    }

    protected void increaseAccessCounter() {
        appConfigService.increaseAccessCount();
    }
}
