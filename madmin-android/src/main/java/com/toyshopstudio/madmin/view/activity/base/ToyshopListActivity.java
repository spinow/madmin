package com.toyshopstudio.madmin.view.activity.base;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 21:37
 */
public abstract class ToyshopListActivity extends ListActivity {

    public static final String EXCEPTIONS_BUNDLE_RES = "string";

    protected static Context context;

    public static Context getContext(){
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = super.getApplication().getApplicationContext();
    }

    protected static final int getExceptionMessageKeyId (String exceptionKey) {
        return context.getResources().getIdentifier(exceptionKey, EXCEPTIONS_BUNDLE_RES, context.getPackageName());
    }

    /**
     * Retrieves a bundled string for an exception.
     * @param exceptionKey The bundled exception message key.
     * @return
     */
    public static final String getExceptionMessageValue (String exceptionKey) {
        return context.getString(getExceptionMessageKeyId(exceptionKey));
    }

}
