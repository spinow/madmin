package com.toyshopstudio.madmin.view.activity.component;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Encapsulates a WebViewClient
 * Created by Saulo Junior on 7/12/2016.
 */
public class MadminWebViewClient extends WebViewClient {

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

}
