package com.toyshopstudio.madmin.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;
import com.toyshopstudio.madmin.view.activity.ActivityEntryList;
import com.toyshopstudio.madmin.view.custom.BoxGraphicBar;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 19/08/14
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */
public class BoxAdapter extends BaseAdapter {

    private Collection<Box> boxes = new ArrayList<Box>(0);
    private Context context;

    public BoxAdapter(Context context, Collection<Box> boxes){
        this.boxes = boxes;
        this.context = context;
        LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return boxes.size();
    }

    @Override
    public Object getItem(int i) {
        return ((ArrayList<Box>) boxes).get(i);
    }

    @Override
    public long getItemId(int i) {
        return i > 0 ? ((ArrayList<Box>) boxes).get(i).getId() : 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = view;
        final ViewHolderBoxItem viewHolder;
        if (view == null) {
            final LayoutInflater li = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.box_list_view_item, null);
            viewHolder = new ViewHolderBoxItem(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderBoxItem) v.getTag();
        }

        final Box box = ((ArrayList<Box>) boxes).get(i);
        viewHolder.boxItemDescription.setText(box.getDescription());
        viewHolder.boxItemValue.setText(box.getFormattedOutgoSum());
        viewHolder.boxImage.setImageResource(resolveImageFor(box));
        viewHolder.boxOutgoesBar.setBarSize(box.getBarSize());
        viewHolder.boxOutgoesBar.setDomain(box);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityEntryList(box, ((ActivityBoxList)context).getSelectedDatePeriod());
            }
        });
        viewHolder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ((ActivityBoxList)context).setSelectedBoxIndex(i+1);
                ((ActivityBoxList)context).openContextMenu(((ActivityBoxList)context).getListView());
                return false;
            }
        });

        return v;
    }

    private void startActivityEntryList(final Box box, final DatePeriod datePeriod) {
        Intent intent = new Intent(context, ActivityEntryList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(ActivityBoxList.SELECTED_BOX_ID, box.getId());
        intent.putExtra(ActivityBoxList.SELECTED_DATE_PERIOD, datePeriod);
        ActivityBoxList.getContext().startActivity(intent);
    }

    private int resolveImageFor(Box box) {
        String outgoSum = box.getFormattedOutgoSum();
        String incomeSum = box.getFormattedIncomeSum();
        if (outgoSum.isEmpty() && incomeSum.isEmpty()) {
            return R.drawable.madmin_empty_box;
        } else if (!outgoSum.isEmpty() && !incomeSum.isEmpty()){
            return R.drawable.madmin_mixed_box;
        } else {
            if (!incomeSum.isEmpty()) {
                return R.drawable.madmin_income_box;
            } else {
                return R.drawable.madmin_outgo_box;
            }
        }
    }

    private class ViewHolderBoxItem {
        TextView boxItemDescription;
        TextView boxItemValue;
        ImageView boxImage;
        BoxGraphicBar boxOutgoesBar;
        public ViewGroup layout;
        ViewHolderBoxItem(View base) {
            boxItemDescription = (TextView) base.findViewById(R.id.txt_box_list_view_item_desc);
            boxItemValue = (TextView) base.findViewById(R.id.txt_box_list_view_item_value);
            boxImage = (ImageView) base.findViewById(R.id.img_box);
            boxOutgoesBar = (BoxGraphicBar) base.findViewById(R.id.img_outgo_bar);
            layout = (ViewGroup) base.findViewById(R.id.box_row_layout);
        }
    }
}
