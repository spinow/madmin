package com.toyshopstudio.madmin.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.EntryType;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Saulo Junior
 * Date: 11/09/14
 * Time: 00:39
 */
public class EntryAdapter extends BaseAdapter {
    Collection<Entry> entrys = new ArrayList<Entry>(0);
    LayoutInflater inflater;
    Context context;

    public EntryAdapter(Context context, Collection<Entry> entrys){
        this.entrys = entrys;
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return entrys.size();
    }

    @Override
    public Object getItem(int i) {
        return ((ArrayList<Entry>)entrys).get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((ArrayList<Entry>)entrys).get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolderEntryItem viewHolderEntryItem;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.entry_list_view_item, null);
            viewHolderEntryItem = new ViewHolderEntryItem(v);
            v.setTag(viewHolderEntryItem);
        } else {
            viewHolderEntryItem = (ViewHolderEntryItem) v.getTag();
        }
        Entry entry = ((ArrayList<Entry>)entrys).get(i);
        viewHolderEntryItem.entryItemDescription.setText(entry.getDescription());
        viewHolderEntryItem.entryItemValue.setText(entry.getFormattedValue());
        if(entry.getType().equals(EntryType.INCOME)) {
            viewHolderEntryItem.entryItemDescription.setTextColor(Color.argb(255,1,100,0));
            viewHolderEntryItem.entryItemValue.setTextColor(Color.argb(255,1,100,0));
        } else {
            viewHolderEntryItem.entryItemDescription.setTextColor(Color.argb(255,135,0,0));
            viewHolderEntryItem.entryItemValue.setTextColor(Color.argb(255,135,0,0));
        }
        return v;
    }

    class ViewHolderEntryItem {
        public TextView entryItemDescription;
        public TextView entryItemValue;
        public ViewHolderEntryItem(View base) {
            entryItemDescription = (TextView) base.findViewById(R.id.txt_entry_list_view_item_desc);
            entryItemValue = (TextView) base.findViewById(R.id.txt_entry_list_view_item_value);
        }
    }
}
