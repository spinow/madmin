package com.toyshopstudio.madmin.view.custom;

import android.content.Context;
import android.widget.FrameLayout;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Domain;

/**
 * Created by Saulo Junior on 19/04/2015.
 */
public class BoxGraphicBar extends GraphicBar {

    private Integer barSize = null;

    public BoxGraphicBar(Context context) {
        super(context);
    }

    public BoxGraphicBar(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public BoxGraphicBar(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void calculateBarSize() {
        if(getDomain() != null) {
            final Box box = (Box)getDomain();
            if(box.getBarSize() == null && this.getBarSize() == null) {
                final int width = ((FrameLayout)this.getParent()).getMeasuredWidth();
                final float percentageInPeriod = box.getOutgoPercentageInPeriod() != null ? box.getOutgoPercentageInPeriod() : 0F;
                this.barSize = Math.round((width / 100F) * percentageInPeriod);
                this.setMinimumHeight(((FrameLayout) this.getParent()).getHeight());
                box.setBarSize(this.barSize);
            }
            if(box.getBarSize() != this.getLayoutParams().width) {
                final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(box.getBarSize(), FrameLayout.LayoutParams.MATCH_PARENT);
                this.setLayoutParams(params);
                if(box.getOutgoMonthlyAverage().floatValue() > 0 && box.getOutgoSum().floatValue() > box.getOutgoMonthlyAverage().floatValue()) {
                    this.setBackgroundResource(R.drawable.box_bar_over_avg_bg);
                } else {
                    this.setBackgroundResource(R.drawable.box_bar_bg);
                }
            }
        }
    }

    public void setBarSize(Integer barSize) {
        this.barSize = barSize;
    }

    public Integer getBarSize() {
        return barSize;
    }
}
