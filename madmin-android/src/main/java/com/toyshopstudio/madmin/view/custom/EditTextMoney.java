package com.toyshopstudio.madmin.view.custom;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * User: Saulo Junior
 * Date: 20/09/14
 * Time: 17:08
 */
public class EditTextMoney extends EditText {

    private boolean updated;

    public EditTextMoney(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextMoney(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextMoney(Context context) {
        super(context);
        init();
    }

    private void init() {
        this.setKeyListener(io_key_listener);

        this.addTextChangedListener (new TextWatcher() {
            public void afterTextChanged (Editable s) {
                String ls_valor_original = s.toString();
                if (updated) {
                    updated = false;
                    return;
                }
                if (ls_valor_original.length() < 16) {

                    StringBuffer mask = new StringBuffer();
                    mask.append(ls_valor_original.replaceAll("[^0-9]*", ""));

                    if(!mask.toString().isEmpty()){
                        Long ln_number = new Long(!mask.toString().isEmpty() ? mask.toString() : "0");
                        mask.replace(0,mask.length(), ln_number.toString());

                        if (mask.length() < 3) {
                            if (mask.length() == 1) {
                                mask.insert(0, "0").insert(0, ",").insert(0, "0");
                            } else if (mask.length() == 2) {
                                mask.insert(0, ",").insert(0, "0");
                            }
                        } else {
                            mask.insert(mask.length() - 2, ",");
                        }

                        if (mask.length() > 6) {
                            mask.insert(mask.length() - 6, '.');

                            if (mask.length() > 10) {
                                mask.insert(mask.length() - 10, '.');

                                if (mask.length() > 14) {
                                    mask.insert(mask.length() - 14, '.');
                                }
                            }
                        }

                        updated = true;

                        EditTextMoney.this.setText(mask);
                        EditTextMoney.this.setSelection(mask.length());
                    }


                }
            }

            public void beforeTextChanged (CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged (CharSequence s, int start, int before, int count) {

            }
        });
    }

    private final KeylistenerNumber io_key_listener = new KeylistenerNumber();

    private class KeylistenerNumber extends NumberKeyListener {

        public int getInputType() {
            return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;

        }

        @Override
        protected char[] getAcceptedChars() {
            return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        }
    }
}