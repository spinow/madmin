package com.toyshopstudio.madmin.view.custom;

import android.content.Context;
import android.widget.ImageButton;
import com.toyshopstudio.madmin.business.Domain;

/**
 * Created by Saulo Junior on 19/04/2015.
 */
public abstract class GraphicBar extends ImageButton {

    private Domain domain;

    public GraphicBar(Context context) {
        super(context);
    }

    public GraphicBar(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphicBar(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        calculateBarSize();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    protected abstract void calculateBarSize();

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(final Domain domain) {
        this.domain = domain;
    }
}
