package com.toyshopstudio.madmin.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.service.BoxService;
import com.toyshopstudio.madmin.infra.data.DataBase;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.BoxDAO;
import com.toyshopstudio.madmin.model.BoxRepository;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.ToastOnTop;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 26/08/14
 * Time: 22:09
 * To change this template use File | Settings | File Templates.
 */
public class DialogAddBox extends MadminDialog {

    BoxService boxService;
    Button btnSaveBox;
    Button btnCancel;
    Dialog dialog;

    public DialogAddBox(final Context context) {
        super(context, R.style.MadminGreyDialog, R.layout.dialog_add_box);
        this.setCancelable(Boolean.TRUE);
        setTitle(context.getString(R.string.title_dialog_add_box));
        DataBase dataBase = SQLiteDataBaseImpl.getInstance();
        EntryDAO entryDAO = EntryDAO.newInstance(dataBase);
        boxService = BoxService.newInstance(
            BoxRepository.newInstance(
                BoxDAO.newInstance(dataBase, entryDAO)
            )
        );
        dialog = this;
        btnSaveBox = ((Button) findViewById(R.id.btn_dialog_add_box));
        btnCancel = ((Button) findViewById(R.id.btn_dialog_cancel_box));
        btnSaveBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String boxName = ((EditText) findViewById(R.id.txt_dialog_add_box)).getText().toString().trim();
                if (boxName == null || boxName.isEmpty()) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_box_name), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else {
                    Box box = boxService.getByDescription(boxName);
                    if(box != null) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_box_unique, Integer.parseInt(context.getString(R.string.maxsize_box_description))), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } else if (boxName.length() > Integer.parseInt(context.getString(R.string.maxsize_box_description))) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_maxsize_box_description, Integer.parseInt(context.getString(R.string.maxsize_box_description))), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } else {
                        box = new Box();
                        box.setDescription(boxName);
                        boxService.save(box);
                        ToastOnMiddle.makeText(context, context.getString(R.string.success_box_created, boxName), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                        ((ActivityBoxList) context).refreshListData(null);
                        dialog.dismiss();
                    }
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onStart() {
        ((EditText)findViewById(R.id.txt_dialog_add_box)).setText("");
        ((EditText)findViewById(R.id.txt_dialog_add_box)).requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
