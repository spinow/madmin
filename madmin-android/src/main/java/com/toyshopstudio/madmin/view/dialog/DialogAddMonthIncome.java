package com.toyshopstudio.madmin.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.Income;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.ToastOnTop;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;

import java.math.BigDecimal;

/**
 * User: Saulo Junior
 * Date: 07/01/2015
 * Time: 14:07
 */
public class DialogAddMonthIncome extends MadminDialog {

    private EntryService entryService;
    private Dialog dialog;

    public DialogAddMonthIncome(final Context context) {
        super(context, R.style.MadminIncomeDialog, R.layout.dialog_add_month_income);
        this.setCancelable(Boolean.TRUE);
        setTitle(context.getString(R.string.title_dialog_add_month_income));
        entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        dialog = this;
        Button btnSaveMonthIncome = ((Button) findViewById(R.id.btn_dialog_add_month_income));
        Button btnCancel = ((Button) findViewById(R.id.btn_dialog_cancel_month_income));
        btnSaveMonthIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entryDescription = ((EditText) findViewById(R.id.txt_add_month_income_description)).getText().toString();
                String entryStringValue = ((EditText) findViewById(R.id.txt_add_month_income_value)).getText().toString();
                if (entryDescription.isEmpty()) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_income_description), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (entryDescription.length() > Integer.parseInt(context.getString(R.string.maxsize_entry_description))) {
                    ToastOnTop.makeText(context, context.getString(R.string.error_maxsize_entry_description, Integer.parseInt(context.getString(R.string.maxsize_entry_description))), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (entryStringValue.isEmpty()) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_income_value), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (BigDecimal.valueOf(Double.parseDouble(entryStringValue.replace(".", "").replace(",", "."))).compareTo(BigDecimal.ZERO) == 0) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_income_value), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else {
                    BigDecimal entryNumberValue;
                    try {
                        entryNumberValue = BigDecimal.valueOf(Double.parseDouble(entryStringValue.replace(".", "").replace(",", ".")));
                        Entry income = new Income();
                        income.setDescription(entryDescription);
                        income.setValue(entryNumberValue);
                        entryService.save(income);
                        ((ActivityBoxList) context).refreshListData(null);
                        dialog.dismiss();
                        ToastOnTop.makeText(context, context.getString(R.string.txt_inserted_entry), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } catch (NumberFormatException e1) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_number_format), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } catch (Exception e2) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_saving_income), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        ((EditText)findViewById(R.id.txt_add_month_income_description)).setText("");
        findViewById(R.id.txt_add_month_income_description).requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        ((EditText) findViewById(R.id.txt_add_month_income_value)).setText("");
    }

}
