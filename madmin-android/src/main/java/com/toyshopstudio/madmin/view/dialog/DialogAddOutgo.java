package com.toyshopstudio.madmin.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.business.Box;
import com.toyshopstudio.madmin.business.Entry;
import com.toyshopstudio.madmin.business.Outgo;
import com.toyshopstudio.madmin.business.service.EntryService;
import com.toyshopstudio.madmin.infra.data.SQLiteDataBaseImpl;
import com.toyshopstudio.madmin.model.EntryDAO;
import com.toyshopstudio.madmin.model.EntryRepository;
import com.toyshopstudio.madmin.view.ToastOnMiddle;
import com.toyshopstudio.madmin.view.ToastOnTop;
import com.toyshopstudio.madmin.view.activity.ActivityEntryList;

import java.math.BigDecimal;

/**
 * User: Saulo Junior
 * Date: 26/08/14
 * Time: 22:09
 */
public class DialogAddOutgo extends MadminDialog {

    private EntryService entryService;
    private Dialog dialog;

    public DialogAddOutgo(final Context context, final Box box) {
        super(context, R.style.ToyShopDialog, R.layout.dialog_add_outgo);
        this.setCancelable(Boolean.TRUE);
        setTitle(context.getString(R.string.title_dialog_add_expense));
        entryService = EntryService.newInstance(EntryRepository.newInstance(EntryDAO.newInstance(SQLiteDataBaseImpl.getInstance())));
        dialog = this;
        Button btnSaveEntry = ((Button) findViewById(R.id.btn_dialog_add_outgo));
        Button btnCancel = ((Button) findViewById(R.id.btn_dialog_cancel_outgo));
        btnSaveEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entryDescription = ((EditText)findViewById(R.id.txt_add_entry_description)).getText().toString();
                String entryStringValue = ((EditText) findViewById(R.id.txt_add_entry_value)).getText().toString();
                if(entryDescription.isEmpty()){
                    ToastOnTop.makeText(context, context.getString(R.string.required_expense_description), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (entryDescription.length() > Integer.parseInt(context.getString(R.string.maxsize_entry_description))){
                    ToastOnTop.makeText(context, context.getString(R.string.error_maxsize_entry_description, Integer.parseInt(context.getString(R.string.maxsize_entry_description))), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (entryStringValue.isEmpty()) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_expense_value), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else if (BigDecimal.valueOf(Double.parseDouble(entryStringValue.replace(".", "").replace(",", "."))).compareTo(BigDecimal.ZERO) == 0) {
                    ToastOnTop.makeText(context, context.getString(R.string.required_expense_value), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                } else {
                    BigDecimal entryNumberValue;
                    try {
                        entryNumberValue = BigDecimal.valueOf(Double.parseDouble(entryStringValue.replace(".", "").replace(",", ".")));
                        final Entry outgo = new Outgo();
                        outgo.setBox(box);
                        outgo.setDescription(entryDescription);
                        outgo.setValue(entryNumberValue);
                        entryService.save(outgo);
                        ((ActivityEntryList)context).refreshListData(box.getId());
                        dialog.dismiss();
                        ToastOnMiddle.makeText(context, context.getString(R.string.txt_inserted_entry), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } catch (NumberFormatException e1) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_number_format), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    } catch (Exception e2) {
                        ToastOnTop.makeText(context, context.getString(R.string.error_saving_expense), Integer.parseInt(context.getString(R.string.app_toast_time))).show();
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        ((EditText)findViewById(R.id.txt_add_entry_description)).setText("");
        findViewById(R.id.txt_add_entry_description).requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        ((EditText) findViewById(R.id.txt_add_entry_value)).setText("");
    }

}
