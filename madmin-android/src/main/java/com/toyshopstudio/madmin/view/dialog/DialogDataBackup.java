package com.toyshopstudio.madmin.view.dialog;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.infra.AccountsTool;
import com.toyshopstudio.madmin.infra.data.backup.*;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;
import com.toyshopstudio.madmin.view.intent.EmailSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 26/08/14
 * Time: 22:09
 * To change this template use File | Settings | File Templates.
 */
public class DialogDataBackup extends MadminDialog {

    private EditText txtEmail;
    private final Dialog dialog;
    private Context context;

    public DialogDataBackup(final Context context) {
        super(context, R.style.MadminGreyDialog, R.layout.dialog_data_backup);
        this.setCancelable(Boolean.TRUE);
        this.context = context;
        setTitle(context.getString(R.string.title_dialog_backup_data));
        dialog = this;
        txtEmail = ((EditText)findViewById(R.id.txt_backup_email));
        txtEmail.setText(AccountsTool.getAccountEmail(AccountManager.get(context)));
        Button btnBackup = ((Button) findViewById(R.id.btn_dialog_data_backup));
        Button btnRestore = ((Button) findViewById(R.id.btn_dialog_data_restore));
        btnBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final BackupData backupData = new SQLiteBackupData(context);
                final IBackupStorer backupStorer = new EmailBackupStorer(buildEmailSender(backupData));
                final DataSecurityEvent backupEvent = new SQLiteBackup(backupData);
                final DataSecurityEvent storeEvent = new SQLiteBackupStore(backupData, backupStorer);
                final List<DataSecurityEvent> events = new ArrayList<DataSecurityEvent>(0);
                events.add(backupEvent);
                events.add(storeEvent);
                dialog.dismiss();
                new BackupDataAsyncTask(context, events).execute();
            }
        });
        btnRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
    }

    /**
     * Sets an EmailSender who'll be responsible for the backup store action.
     * @param data a reference for the path string stored into the backup data file (filled when the user chooses a file).
     * @return a new instance of an EmailSender
     */
    private EmailSender buildEmailSender (BackupData data) {
        EmailSender email = new EmailSender(this.context);
        email.addRecipients(txtEmail.getText().toString());
        if(data != null) {
            email.setBackupData(data);
        }
        email.setSubject(context.getString(R.string.title_email_sender_backup_data));
        return email;
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            ((Activity)this.context).startActivityForResult(
                    Intent.createChooser(intent, this.context.getString(R.string.txt_backup_chose_file)),
                    ActivityBoxList.FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this.context, this.context.getString(R.string.exception_backup_file_no_file_manager),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
