package com.toyshopstudio.madmin.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.infra.DatePeriod;
import com.toyshopstudio.madmin.view.activity.ActivityBoxList;
import com.toyshopstudio.madmin.view.activity.base.ToyshopActivity;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Saulo Junior
 * Date: 06/12/14
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */
public class DialogSelectPeriod extends MadminDialog {

    Spinner spnYear;
    Spinner spnMonth;
    Button btnCancel;
    Button btnConfirm;
    Dialog dialog;

    public DialogSelectPeriod(final Context context, DatePeriod period) {
        super(context, R.style.MadminGreyDialog, R.layout.dialog_select_period);
        this.setCancelable(Boolean.TRUE);
        setTitle(context.getString(R.string.title_dialog_select_period));
        dialog = this;
        Map yearMap = getYearMap();
        spnYear = ((Spinner) findViewById(R.id.spinner_select_period_year));
        spnYear.setAdapter(new ArrayAdapter<String>(context, R.layout.madmin_spinner_item, getYearList(yearMap)));
        spnYear.setSelection(getCurrentYearIndex(period, yearMap) - 1);
        spnMonth = ((Spinner) findViewById(R.id.spinner_select_period_month));
        spnMonth.setAdapter(ArrayAdapter.createFromResource(context,R.array.months_array, R.layout.madmin_spinner_item));
        spnMonth.setSelection(getCurrentMonth(period));
        btnCancel = ((Button) findViewById(R.id.btn_dialog_cancel_select_period));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnConfirm = ((Button) findViewById(R.id.btn_dialog_confirm_select_period));
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePeriod datePeriod = getDatePeriod();
                Intent selectedDatePeriodIntent = new Intent(ToyshopActivity.SELECTED_DATE_PERIOD_INTENT);
                selectedDatePeriodIntent.putExtra(ActivityBoxList.SELECTED_DATE_PERIOD, datePeriod);
                dialog.dismiss();
                context.sendBroadcast(selectedDatePeriodIntent);
            }
        });
    }

    private DatePeriod getDatePeriod() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(GregorianCalendar.YEAR, Integer.valueOf(spnYear.getSelectedItem().toString()));
        calendar.set(GregorianCalendar.MONTH, Integer.valueOf(Integer.parseInt(String.valueOf(spnMonth.getSelectedItemId()))));
        calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date initialDate = calendar.getTime();
        calendar.setTime(initialDate);
        calendar.set(GregorianCalendar.DAY_OF_MONTH, calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        Date finalDate = calendar.getTime();
        return new DatePeriod(initialDate, finalDate);
    }

    private Integer getCurrentYearIndex(DatePeriod period, Map<Integer, String> yearMap) {
        Integer index = 0;
        Date date = period != null ? period.getInitialDate() : new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        Integer year = calendar.get(GregorianCalendar.YEAR);
        for(Map.Entry<Integer, String> entry : yearMap.entrySet()) {
            if(entry.getValue().equals(year.toString())) {
                index = entry.getKey();
                break;
            }
        }
        return index;
    }

    private Integer getCurrentMonth(DatePeriod period) {
        Date date = period != null ? period.getInitialDate() : new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        Integer month = calendar.get(GregorianCalendar.MONTH);
        return month;
    }

    private List<String> getYearList(Map<Integer, String> years) {
        Collection<String> yearList = years.values();
        List<String> yearStrings = new ArrayList<String>(0);
        for (String year : yearList) {
            yearStrings.add(year);
        }
        return yearStrings;
    }

    private Map<Integer, String> getYearMap() {
        Map<Integer, String> map = new HashMap<Integer, String>(0);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        Integer year = calendar.get(GregorianCalendar.YEAR);
        Integer index = 1;
        while (year >= 2014) {
            map.put(index, year.toString());
            year--;
            index++;
        }
        return map;
    }

    @Override
    protected void onStart() {
    }
}
