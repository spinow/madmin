package com.toyshopstudio.madmin.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Encapsulates an app patternized Dialog
 * Created by Saulo Junior on 7/25/2016.
 */
class MadminDialog extends Dialog {

    MadminDialog(Context context, int theme, int layout) {
        super(context, theme);
        setContentView(layout);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = (int) (getDeviceMetrics(context).widthPixels*0.9);
        getWindow().setAttributes(params);
    }

    public MadminDialog(Context context) {
        super(context);
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }
}
