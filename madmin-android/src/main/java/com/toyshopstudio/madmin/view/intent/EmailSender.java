package com.toyshopstudio.madmin.view.intent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.toyshopstudio.madmin.R;
import com.toyshopstudio.madmin.infra.data.backup.BackupData;
import com.toyshopstudio.madmin.infra.data.backup.SQLiteBackupData;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Saulo Júnior
 * Date: 10/1/15
 * Time: 6:15 PM
 */
public class EmailSender {

    private Context context;
    private List<String> recipients = new ArrayList<String>(0);
    private BackupData backupData;
    private String subject;
    private String intentTitle;

    private EmailSender () {}

    public EmailSender(final Context context) {
        this.context = context;
    }

    public String send () throws EmailSenderException {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("vnd.android.cursor.dir/email");
        try {
            if (this.recipients == null || this.recipients.isEmpty()) {
                throw new EmailSenderException("exception_email_sender_no_recipient");
            } else if (this.subject == null || this.subject.isEmpty()) {
                throw new EmailSenderException("exception_email_sender_no_subject");
            }
            intent.putExtra(Intent.EXTRA_EMAIL, getRecipients());
            intent.putExtra(Intent.EXTRA_SUBJECT, this.subject);
            if(((SQLiteBackupData)this.backupData).getFile() != null) {
                Uri uri = Uri.parse("file://" + ((SQLiteBackupData)this.backupData).getFile().getPath());
                intent.putExtra(Intent.EXTRA_STREAM, uri);
            }
            if(this.intentTitle == null || this.intentTitle.isEmpty()) {
                this.intentTitle = context.getString(R.string.txt_email_sender_intent_title);
            }
            context.startActivity(Intent.createChooser(intent, this.intentTitle));
        } catch (EmailSenderException e) {
            Log.e(this.getClass().getSimpleName(),e.getMessage());
            return e.getMessage();
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(),e.getMessage());
            return context.getString(R.string.backup_fail);
        }
        return context.getString(R.string.success_backup_created);
    }

    public String[] getRecipients() {
        int idx = 0;
        String[] emails = new String[recipients.size()];
        for ( String recipient : recipients ) {
            emails[idx] = recipient;
        }
        return emails;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public void addRecipients(String recipient) {
        this.recipients.add(recipient);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIntentTitle() {
        return intentTitle;
    }

    public void setIntentTitle(String intentTitle) {
        this.intentTitle = intentTitle;
    }

    public BackupData getBackupData() {
        return backupData;
    }

    public void setBackupData(BackupData backupData) {
        this.backupData = backupData;
    }
}
