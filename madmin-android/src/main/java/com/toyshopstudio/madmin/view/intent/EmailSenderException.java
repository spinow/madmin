package com.toyshopstudio.madmin.view.intent;

/**
 * User: Saulo Júnior
 * Date: 10/1/15
 * Time: 6:21 PM
 */
public class EmailSenderException extends Exception {

    EmailSenderException(String message) {
        super(message);
    }

}
